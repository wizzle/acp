<?php

use yii\db\Migration;

class m160309_090514_create_record_info_table extends Migration
{
    public function up()
    {
        $this->createTable('record_info', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer(),
            'description' => $this->text(),
            'color' => $this->string(20),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey('fk-record_if-acctid', '{{%record_info}}', 'record_id', '{{%cdr}}', 'acctid', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('record_info');
    }
}
