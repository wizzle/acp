<?php

use yii\db\Schema;
use yii\db\Migration;

class m160302_124004_create_acp_profile_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%cdr_profile}}', [
            'user_id'        => Schema::TYPE_INTEGER . ' PRIMARY KEY',
            'inner_phone'    => Schema::TYPE_STRING . '(11)',
            'sip_account'    => Schema::TYPE_STRING . '(32)',
        ]);
        $this->addForeignKey('fk_user_cdr_profile', '{{%cdr_profile}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
//        $this->addForeignKey('fk_group_id_cdr_profile', '{{%cdr_profile}}', 'sip_group_id', '{{%sip_group}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%cdr_profile}}');
    }
}
