 up<?php

use yii\db\Migration;

class m160325_063707_create_cdr_answered_only extends Migration
{
    public function up()
    {
        $this->createTable('{{%cdr_answered_only}}', [
            'id' => $this->primaryKey(),
            'cdr_acctid' => $this->integer(10)->notNull(),
            'call_date' => $this->integer(10)->notNull(),
            'duration' => $this->integer()->notNull(),
            'type' => 'ENUM(\'1\', \'2\') NOT NULL DEFAULT \'1\'', // 1 incoming call, 2 outgoing call
            'dcontext' => $this->string()->defaultValue(null),
            'client_phone' => $this->string('14')->defaultValue(null),
            'operator_id' => $this->integer()->defaultValue(0),
            'sip_account' => $this->string(20)->defaultValue(null),
            'record_file_path' => $this->string()->defaultValue(null),
            'import' => $this->integer()->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        $this->createIndex('cdr_answered_only_call_date', '{{%cdr_answered_only}}', 'call_date');
        $this->createIndex('cdr_answered_only_dcontext', '{{%cdr_answered_only}}', 'dcontext');
        $this->createIndex('cdr_answered_only_client_phone', '{{%cdr_answered_only}}', 'client_phone');
        $this->createIndex('cdr_answered_only_operator_id', '{{%cdr_answered_only}}', 'operator_id');
        $this->createIndex('cdr_answered_only_sip_account', '{{%cdr_answered_only}}', 'sip_account');

        $sql = '
CREATE TRIGGER `cdr_after_insert` AFTER INSERT ON `cdr` FOR EACH ROW BEGIN
    DECLARE unix_call_time, call_type, operator_id INTEGER DEFAULT 1;
    DECLARE client_phone, sip_acc VARCHAR(16) DEFAULT NULL;
    IF (INSTR(NEW.disposition, \'ANSWERED\')>0 AND NEW.recordfile IS NOT NULL)
        THEN
        SET unix_call_time = UNIX_TIMESTAMP(NEW.calldate);
        SET call_type = IF (CHAR_LENGTH(NEW.src) > 4, 1, 2); 
        SET client_phone = IF (CHAR_LENGTH(NEW.src) > 4, REPLACE(NEW.src, \'+375\', \'80\'), NEW.dst);
        SET client_phone = IF (CHAR_LENGTH(client_phone) = 10, CONCAT(REPLACE(LEFT(client_phone,1), \'8\', \'80\'), SUBSTRING(client_phone, 2, CHAR_LENGTH(client_phone))), client_phone);
        SET sip_acc = IF (CHAR_LENGTH(NEW.src) > 4, SUBSTRING_INDEX(NEW.dstchannel, \'-\', 1), SUBSTRING_INDEX(NEW.channel, \'-\', 1));
        SET operator_id = (SELECT user_id FROM cdr_profile WHERE cdr_profile.sip_account = sip_acc);
        INSERT INTO `cdr_answered_only` SET `cdr_acctid` = NEW.acctid , `call_date` = unix_call_time, `duration` = NEW.billsec, `type` = call_type, `dcontext` = NEW.dcontext, `client_phone` = client_phone, `operator_id` = operator_id, `sip_account` = sip_acc, `record_file_path` = NEW.recordfile;
    END IF;
END
        ';
        $this->execute($sql);
    }

    public function down()
    {
        $sql = 'DROP TRIGGER IF EXISTS `cdr_after_insert`;';
        $this->execute($sql);
        $this->dropTable('cdr_answered_only');
    }
}
