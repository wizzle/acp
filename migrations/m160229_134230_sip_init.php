<?php

use yii\db\Migration;
use yii\rbac\DbManager;
use yii\base\InvalidConfigException;


class m160229_134230_sip_init extends Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    public function up()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sip_group}}', [
            'id' => $this->primaryKey(),
            'group_name' => $this->string(64)->notNull(),
            'description' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
        $this->createIndex('sip_group_unique_group_name', '{{%sip_group}}', 'group_name', true);


        $this->createTable('{{%sip_context}}', [
            'id' => $this->primaryKey(),
            'context_name' => $this->string(64)->notNull(),
            'description' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
        $this->createIndex('idx-context_name', '{{%sip_context}}', 'context_name');

        $this->createTable('{{%sip_user_group}}', [
            'user_id' => $this->integer(),
            'group_id' => $this->integer(),
            'PRIMARY KEY (user_id, group_id)',
        ], $tableOptions);
        $this->createIndex('idx-sip_user_group-user_id', '{{%sip_user_group}}', 'user_id');
        $this->createIndex('idx-sip_user_group-group_id', '{{%sip_user_group}}', 'group_id');
        $this->addForeignKey('fk-sip_user_group-user_id', '{{%sip_user_group}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-sip_user_group-group_id', '{{%sip_user_group}}', 'group_id', '{{%sip_group}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('sip_context_sip_group', [
            'sip_context_id' => $this->integer(),
            'sip_group_id' => $this->integer(),
            'PRIMARY KEY(sip_context_id, sip_group_id)'
        ]);

        $this->createIndex('idx-sip_context_sip_group-sip_context_id', 'sip_context_sip_group', 'sip_context_id');
        $this->createIndex('idx-sip_context_sip_group-sip_group_id', 'sip_context_sip_group', 'sip_group_id');

        $this->addForeignKey('fk-sip_context_sip_group-sip_context_id', 'sip_context_sip_group', 'sip_context_id', 'sip_context', 'id', 'CASCADE');
        $this->addForeignKey('fk-sip_context_sip_group-sip_group_id', 'sip_context_sip_group', 'sip_group_id', 'sip_group', 'id', 'CASCADE');

        $this->batchInsert('{{%sip_group}}', [
            'group_name',
            'description',
        ],
            [
                [
                    'sales',
                    'Group for sales managers.',
                ],
                [
                    'buhgalteria',
                    'Group for buhgalters.',
                ],
                [
                    'context-managers',
                    'Group for context managers.',
                ],
            ]
        );

        $this->batchInsert('{{%sip_context}}', [
            'context_name',
            'description',
            ],
            [
                [
                    'incoming',
                    'Context for all incoming calls from pstn',
                ],
                [
                    'incoming-mobile',
                    'Context for all incoming calls from mobile gateways',
                ],
                [
                    'pstn',
                    'Context for all outgoing calls to pstn',
                ],
                [
                    'mobile',
                    'Context for all outgoing calls to gsm numbers',
                ],
            ]
        );



    }

    public function fill()
    {

    }

    public function down()
    {
        $this->dropTable('{{%sip_context_sip_group}}');
        $this->dropTable('{{%sip_user_group}}');
        $this->dropTable('{{%sip_context}}');
        $this->dropTable('{{%sip_group}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
