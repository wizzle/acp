<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.02.2016
 * Time: 20:16
 */

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class Cdr extends ActiveRecord
{
    const LISTENED = 1;
    const NOT_LISTENED = 0;

    protected static $contexts = [
        'incoming',
        'incoming-mobile',
        'mobile',
        'pstn',
    ];

    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return 'cdr';
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'acctid'           => Yii::t('app', 'Rec ID'),
            'calldate'         => Yii::t('app', 'Date and Time'),
            'billsec'          => Yii::t('app', 'Duration'),
            'src'              => Yii::t('app', 'Call Source'),
            'dst'              => Yii::t('app', 'Call Destination'),
            'recordfile'       => Yii::t('app', 'File Path'),
//            'website'        => Yii::t('user', 'Website'),
//            'bio'            => Yii::t('user', 'Bio'),
        ];
    }


    public static function getCallDetailRecordsForUser($usersInnerPhones = [], $date, $contexts = [])
    {

        $query = static::find()
            ->where(['or like', 'channel', $usersInnerPhones,])
            ->orWhere(['or like', 'dstchannel', $usersInnerPhones])
            ->andWhere(['>', 'billsec',10])
            ->andWhere(['between', 'calldate',$date.' 00:00:00', $date.' 23:59:59'])
            ->andWhere(['disposition' => 'answered'])
        ;

        if (!empty($contexts)) {
            $query->andWhere(['in', 'dcontext', self::$contexts]);
        }

//        $command = $query->createCommand();
//        echo($command->rawSql);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getDatesOfCalls($usersInnerPhones = [], $date, $contexts = [])
    {

        $query = static::find()->select('date(calldate) as callsDate, count(*) as callsCount')
            ->groupBy('callsDate')
            ->where(['or like', 'channel', $usersInnerPhones,])
            ->orWhere(['or like', 'dstchannel', $usersInnerPhones])
            ->andWhere(['>', 'billsec',10])
            ->andWhere(['disposition' => 'answered'])
            ->andWhere(['in', 'dcontext', self::$contexts])->asArray()->all()
        ;
//        $command = $query->createCommand()->rawSql;
        return $query;
    }


    public static function getCallDetailRecordsById($id)
    {
        $query = static::find()->where(['acctid' => $id])->one();
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    public function getCallDetailRecordsForClientNumber($clientNumber)
    {

    }
}