<?php


namespace app\models\treeview;


use yii\helpers\Url;

class TreeViewGenerator
{
    private $years = [];
    private $months = [];
    private $days = [];

    private $currentDay;
    private $currentMonth;
    private $currentYear;

    private $previousDay;
    private $previousMonth;
    private $previousYear;

    private $callsCount;

    private $currentUrl;

    public function addItem($model, $url)
    {
        $this->currentUrl = $url;
        list($this->currentYear, $this->currentMonth, $this->currentDay) = explode('-', $model['callsDate'], 3);
        $this->callsCount = $model['callsCount'];
        if ($this->previousDay > $this->currentDay) {
            $this->addMonthItem();
        }
        $this->addDayItem();
    }

    private function addDayItem()
    {
        $date = $this->currentYear.'-'.$this->currentMonth.'-'.$this->currentDay;
        $href = $this->currentUrl ? Url::current(['date' => $date]) : Url::to(['date' => $date]);
        $this->days[] = [
            'text' => $this->currentDay,
//            'state' => ['disabled'=>'true'],
            'tags' => [$this->callsCount],
            'href' => $href,
        ];
        $this->previousMonth = $this->currentMonth;
        $this->previousYear = $this->currentYear;
        return $this->previousDay = $this->currentDay;
    }

    private function addMonthItem()
    {
        $this->months[] = [
            'text' => $this->previousMonth,
//            'tags' => [$monthCallsCount],

            'nodes' => $this->days,
        ];
        if ($this->previousYear != $this->currentYear) {
            $this->addYearItem();
        }
        unset($this->days);
        return $this->previousMonth = $this->currentMonth;

    }
    private function addYearItem()
    {
        $this->years[] = [
            'text' => $this->previousYear,
            'nodes' => $this->months,
        ];
        unset($this->months);
        $this->previousMonth = $this->currentMonth;
        return $this->previousDay = $this->currentDay;
    }

    public function getItems()
    {
        $this->months[] = [
            'text' => $this->currentMonth,
            'state' => ['expanded' => 'true'], //expand the current month in view
//            'tags' => [$monthCallsCount],
            'nodes' => $this->days,
        ];
        $this->years[] = [
            'text' => $this->currentYear,
            'nodes' => $this->months,
        ];


        return $this->years;
    }

}