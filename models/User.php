<?php
namespace app\models;
use Yii;
use dektrium\user\models\User as BaseUser;

/**
 * Class User
 * @package app\models
 * Database fields:
 * @property integer $user_id
 * @property string  $inner_phone
 * @property string  $sip_account
 *
 * Defined relations:
 * @property Account[] $accounts
 * @property CdrProfile   $cdrProfile
 *
 */
class User extends BaseUser
{
    /** @var CdrProfile|null */
    private $_cdrProfile;

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $auth = Yii::$app->authManager;
            $defaultRole = $auth->getRole('user');
            $auth->assign($defaultRole, $this->getId());
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCdrProfile()
    {
        return $this->hasOne($this->module->modelMap['CdrProfile'], ['user_id' => 'id']);
    }

    /**
     * @param CdrProfile $cdrProfile
     */
    public function setProfile(CdrProfile $cdrProfile)
    {
        $this->_cdrProfile = $cdrProfile;
    }

}