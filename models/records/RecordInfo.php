<?php

namespace app\models\records;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "record_info_table".
 *
 * @property integer $id
 * @property integer $record_id
 * @property string $description
 * @property string $color
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cdr $record
 */
class RecordInfo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'record_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['color'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'record_id' => Yii::t('app', 'Record ID'),
            'description' => Yii::t('app', 'Description'),
            'color' => Yii::t('app', 'Color'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Cdr::className(), ['acctid' => 'record_id']);
    }
}
