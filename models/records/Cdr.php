<?php

namespace app\models\records;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "cdr".
 *
 * @property integer $acctid
 * @property string $src
 * @property string $dst
 * @property string $calldate
 * @property string $clid
 * @property string $dcontext
 * @property string $channel
 * @property string $dstchannel
 * @property string $lastapp
 * @property string $lastdata
 * @property integer $duration
 * @property integer $billsec
 * @property string $disposition
 * @property string $amaflags
 * @property integer $accountcode
 * @property string $uniqueid
 * @property string $userfield
 * @property string $recordfile
 * @property integer $import_cdr
 */
class Cdr extends ActiveRecord
{
    /**
     * @var string time in H:i:s format.
     */
    public $call_duration;

    /**
     * @var string 'in' or 'out'.
     */
    public $direction;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cdr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['calldate'], 'required'],
            [['calldate'], 'safe'],
            [['duration', 'billsec', 'accountcode', 'import_cdr'], 'integer'],
            [['src', 'dst', 'clid', 'dcontext', 'channel', 'dstchannel', 'lastapp', 'lastdata', 'disposition', 'amaflags', 'userfield'], 'string', 'max' => 80],
            [['uniqueid'], 'string', 'max' => 32],
            [['recordfile'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'acctid' => Yii::t('app', 'Record Number'),
            'src' => Yii::t('app', 'Src'),
            'dst' => Yii::t('app', 'Dst'),
            'calldate' => Yii::t('app', 'Calldate'),
            'clid' => Yii::t('app', 'Clid'),
            'dcontext' => Yii::t('app', 'Dcontext'),
            'channel' => Yii::t('app', 'Channel'),
            'dstchannel' => Yii::t('app', 'Dstchannel'),
            'lastapp' => Yii::t('app', 'Lastapp'),
            'lastdata' => Yii::t('app', 'Lastdata'),
            'duration' => Yii::t('app', 'Duration'),
            'billsec' => Yii::t('app', 'Billsec'),
            'disposition' => Yii::t('app', 'Disposition'),
            'amaflags' => Yii::t('app', 'Amaflags'),
            'accountcode' => Yii::t('app', 'Accountcode'),
            'uniqueid' => Yii::t('app', 'Uniqueid'),
            'userfield' => Yii::t('app', 'Userfield'),
            'recordfile' => Yii::t('app', 'Recordfile'),
            'import_cdr' => Yii::t('app', 'Import Cdr'),
            'call_duration' => Yii::t('app', 'Call Duration'),
            'direction' => Yii::t('app', 'Call Direction')
        ];
    }

    /**
     * хз на сколько это правильно, но раньше у меня этими преобразованиями
     * занималась вьюха, и мне это очень не нравилось
     * @return string
     */
    public function getClientPhone()
    {
        //https://github.com/giggsey/libphonenumber-for-php
        $phoneNumber = strlen($this->src) > 4 ? $this->src : $this->dst;
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $phoneNumberObject = $phoneUtil->parse($phoneNumber, "BY");
            return $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::NATIONAL);
        } catch (NumberParseException $e) {
            return $phoneNumber;
        }
    }
}
