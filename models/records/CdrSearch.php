<?php

namespace app\models\records;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CdrSearch represents the model behind the search form about `app\models\records\Cdr`.
 */
class CdrSearch extends Cdr
{
//    public $call_duration;
    public $phone;
    public $channel;
    public $callDateStart;
    public $callDateEnd;

    const SCENARIO_SEARCH_MULTIPLE_RECORDS = 'search';
//    const SCENARIO_SEARCH_RECORD_BY_ID = 'searchById';
    const SCENARIO_SEARCH_SRC_OR_DST_BY_PHONE = 'searchByPhone';

    public function formName()
    {
        return 'search';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['calldate', 'date', 'format' => 'Y-m-d', 'skipOnEmpty' => true, 'on' => [self::SCENARIO_SEARCH_MULTIPLE_RECORDS, ]],
            [['channel', 'dstchannel', 'dcontext'], 'safe', 'on' => [self::SCENARIO_SEARCH_MULTIPLE_RECORDS, ]],
            [['acctid', 'phone'], 'integer', /*'on' => self::SCENARIO_SEARCH_RECORD_BY_ID*/],
            [['phone'], 'integer', 'on' => [self::SCENARIO_SEARCH_SRC_OR_DST_BY_PHONE, self::SCENARIO_SEARCH_MULTIPLE_RECORDS,]],
            [['channel'], 'safe', 'on' => [self::SCENARIO_SEARCH_SRC_OR_DST_BY_PHONE, ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
//        return Model::scenarios();
        $scenarios = Model::scenarios();
        $scenarios[self::SCENARIO_SEARCH_MULTIPLE_RECORDS] = ['calldate', 'channel', 'dstchannel', 'dcontext', 'phone'];
//        $scenarios[self::SCENARIO_SEARCH_RECORD_BY_ID] = ['acctid'];
        $scenarios[self::SCENARIO_SEARCH_SRC_OR_DST_BY_PHONE] = ['phone', 'channel'];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /**
         * по тз было требование сортировки по полю длительность.
         * Т.к. во вьюхе неприемлемо отображение времени разговора
         * в секундах, то вычисление и преобразование времени в поле billsec
         * было переложено на MySQL.
         * Были введены свойства call_duration и direction в родительскую модель.
         */
        $query = self::find()
            ->select([
                '*',
                'SEC_TO_TIME(`billsec`) as `call_duration`',
                'IF(LENGTH(`src`) > 4, \'in\', \'out\') as `direction`',
            ])
            ->where(['>', 'billsec',10]) // нам интересны только вызовы длительностью более 10 секунд
            ->andWhere(['disposition' => 'answered']) // которые состоялись
            ->andWhere(['in', 'dcontext', $this->dcontext]) // которые входят только в перечисленные контексты.
        ;
        $pagination = $this->channel ? ['pageSize' => 50,] : false;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
        ]);
        /**
         * переопределение метода сортировки необходимо т.к. поле call_duration вычисляемое.
         */
        $dataProvider->setSort([
            'attributes' => [
                'calldate',
                'call_duration' => [
                    'asc' => ['billsec' => SORT_ASC],
                    'desc' => ['billsec' => SORT_DESC],
                    'label' => 'Call Duration',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $startDate = $this->calldate ? $this->calldate .' 00:00:00' : '';
        $endDate = $this->calldate ? $this->calldate .' 23:59:59' : '';
        $query->andFilterWhere(['between', 'calldate', $startDate, $endDate]); // которые звонили сегодня если не указано иной даты
        $query->andFilterWhere(['or', ['or like', 'channel', $this->channel], ['or like', 'dstchannel', $this->channel]]);
        $query->andFilterWhere(['or', ['or like', 'src', $this->phone], ['or like', 'dst', $this->phone]]);
//        var_dump($query->createCommand()->rawSql, $this->validate()); exit;
        return $dataProvider;
    }

    public function searchByPhone($params)
    {
        $this->phone = $params['phone'];
        $this->channel = $params['channel'];

        if (!$this->validate()) {
            return [];
        }

        $query = self::find()
            ->select([
                'IF(INSTR(src, \''. $this->phone .'\'), src, dst) as phone'
            ])
            ->groupBy(['phone']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere(['or', ['or like', 'src', $this->phone], ['or like', 'dst', $this->phone]]);
        $query->andFilterWhere(['or', ['or like', 'channel', $this->channel], ['or like', 'dstchannel', $this->channel]]);
        //var_dump($this->validate(), $this->phone, $query->createCommand()->rawSql); exit;
        return $query->limit('10')->column();

    }

    public function searchById($params)
    {
        $query = Cdr::find()->select([
            '*',
            'SEC_TO_TIME(`billsec`) as `call_duration`',
            'IF(LENGTH(`src`) > 4, \'in\', \'out\') as `direction`',
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere(['acctid' => $this->acctid]);

        return $dataProvider->query->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalInfo()
    {
        return $this->hasOne(RecordInfo::className(), ['record_id' => 'acctid']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getRecordInfo()
    {
        return $this->hasOne(RecordInfo::className(), ['record_id' => 'acctid'])->one();
    }

}
