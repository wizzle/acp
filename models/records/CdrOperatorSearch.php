<?php

namespace app\models\records;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
* @property array $dcontext Contexts which include incoming and outgoing calls. NOT INTERNAL
*/
class CdrOperatorSearch extends Cdr
{
    public function formName()
    {
        return 'search';
    }

    /**
     * Search only by this fields
     * If 'calldate' is empty skip this and set 'calldate' to today
     * @return array
     */
    public function rules()
    {
        return[
            ['calldate', 'date', 'skipOnEmpty' => true],
            ['calldate', 'default', 'value' => date('Y-m-d', time())],
            [['channel', 'dstchannel'], 'safe'],
            [['dcontext'], 'default', 'value' => null],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        /**
         * валидируем и загружаем посторонний ввод именно здесь т.к.
         * валидация еще и дату заполняет если не указана.
         * Может это не верно?
         */
        $isLoaded = $this->load($params);
        $isValidated = $this->validate();

        $query = Cdr::find()
            ->where(['>', 'billsec',10]) // нам интересны только вызовы длительностью более 10 секунд
            ->andWhere(['disposition' => 'answered']) // которые состоялись
            ->andWhere(['in', 'dcontext', $this->dcontext]) // которые входят только в перечисленные контексты.
            ->andWhere(['between', 'calldate',$this->calldate.' 00:00:00', $this->calldate.' 23:59:59']) // которые звонили сегодня если не указано иной даты
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        if (!($isLoaded and $isValidated)){
            return $dataProvider;
        }
        //здесь собираем в кучку список операторов
        $query->andFilterWhere(['or', ['or like', 'channel', $this->channel], ['or like', 'dstchannel', $this->channel]]);
        return $dataProvider;
    }

    private function baseSearch()
    {
        return Cdr::find()
            ->where(['>', 'billsec',10]) // нам интересны только вызовы длительностью более 10 секунд
            ->andWhere(['disposition' => 'answered']) // которые состоялись
            ->andWhere(['in', 'dcontext', $this->dcontext]) // которые входят только в перечисленные контексты.
            ->andWhere(['between', 'calldate',$this->calldate.' 00:00:00', $this->calldate.' 23:59:59']) // которые звонили сегодня если не указано иной даты
            ;
    }
}