<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11.02.2016
 * Time: 21:53
 */

namespace app\models;
use app\models\sipconfig\SipGroup;
use app\models\sipconfig\SipUserGroup;
use Yii;
use dektrium\user\models\Profile as BaseProfile;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;


class CdrProfile extends BaseProfile
{
    /** @property string $groups */
    public $groups = [];
    /** @var \dektrium\user\Module */
    protected $module;

    /** @inheritdoc */
    public function init()
    {
        $this->module = Yii::$app->getModule('user');
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return '{{%cdr_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'innerPhone' => ['inner_phone', 'string', 'max' => 11],
            'sipAccount' => ['sip_account', 'string', 'max' => 32],
            ['groups', 'default', 'value' => $this->getUserGroups()],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'inner_phone'    => Yii::t('user', 'Internal phone number'),
            'sip_account'    => Yii::t('user', 'SIP account from sip.conf'),
        ];
    }


    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isAttributeChanged('sip_account')) {
                $this->setAttribute('sip_account', strtoupper($this->getAttribute('sip_account')));
            }

            return true;
        }

        return false;
    }

    /**
     * http://sllite.ru/2014/11/yii2-%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B1%D0%BB%D0%BE%D0%B3%D0%B0-%D1%87%D0%B0%D1%81%D1%82%D1%8C-6-%D1%82%D1%8D%D0%B3%D0%B8-%D0%B4%D0%BB%D1%8F-%D0%BF%D0%BE%D1%81%D1%82/
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        if(is_array($this->groups)) {
            SipUserGroup::deleteAll(['user_id' => $this->user_id]);
            $values = [];
            foreach ($this->groups as $group_id) {
                $values[] = [$this->user_id, $group_id];
            }
            self::getDb()->createCommand()
                ->batchInsert(SipUserGroup::tableName(), ['user_id', 'group_id'], $values)->execute();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUser()
    {
        return $this->hasOne($this->module->modelMap['User'], ['id' => 'user_id']);
    }

    /**
     * Returns all available group items to be attached to user.
     * @return array
     */
    public function getAvailableGroups()
    {
        return ArrayHelper::map(SipGroup::find()->all(), 'id', function ($item) {
            return empty($item->description)
                ? $item->group_name
                : $item->group_name . ' (' . $item->description . ')';
        });
    }

    /**
     * @link http://ru.stackoverflow.com/questions/227639/%D1%81%D0%B2%D0%B5%D1%80%D0%BD%D1%83%D1%82%D1%8C-%D0%B2%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9-%D0%BC%D0%B0%D1%81%D1%81%D0%B8%D0%B2-%D0%B2-%D0%BE%D0%B4%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%BD%D1%8B%D0%B9
     * @return \yii\db\ActiveQuery
     */
    public function getAllContexts()
    {
        $contexts = [];
        $groups = $this->getUserGroups()->all();
        foreach ($groups as $group) {
            $contexts[] = ArrayHelper::map($group->contexts, 'id', 'context_name');
        };

        $contextsIterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($contexts));

        $result =[];
        foreach ($contextsIterator as $context) {
            if (!in_array($context, $result)) $result[] = $context;
        }

        return $result;
    }

    /**
     * Здесь проблема в том что группы достаются магией
     * через метод getUserGroups() в модели,
     * а сохраняются группы в связанную модель
     * из переменной groups методом updateGroups который вызывается из контроллера.
     * Т.е. если в модели создать публичное свойство
     * groups и метод getGroups, то инпут будет пустой,
     * но изменения все равно будут сохраняться
     * если же публичное свойство не создавать, то при сохранении формы
     * получаем исключение - переменная groups readonly
     */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGroups()
    {
        //$this->groups = $this->hasMany(SipGroup::className(), ['id' => 'group_id'])->viaTable('sip_user_group', ['user_id' => 'user_id']);
        return $this->hasMany(SipGroup::className(), ['id' => 'group_id'])->viaTable('sip_user_group', ['user_id' => 'user_id']);
    }

    public static function getUserIdByInnerPhone($phone)
    {
        return static::find()->select('user_id')->where(['inner_phone' => $phone])->one()->user_id;
    }

    public static function getSipAccounts()
    {
        $query = static::find()->all();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }
}