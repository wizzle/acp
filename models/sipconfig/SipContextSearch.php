<?php

namespace app\models\sipconfig;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\sipconfig\SipContext;

/**
 * SipContextSearch represents the model behind the search form about `app\models\sipconfig\SipContext`.
 */
class SipContextSearch extends SipContext
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['context_name', 'description'], 'safe'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SipContext::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'context_name', $this->context_name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
