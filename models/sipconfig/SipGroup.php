<?php

namespace app\models\sipconfig;

use app\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sip_group".
 *
 * @property integer $id
 * @property string $group_name
 * @property integer $created_at
 *
 * @property SipUserGroup[] $sipUserGroups
 * @property User[] $users
 */
class SipGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_name'], 'required'],
            [['created_at'], 'integer'],
            [['group_name'], 'string', 'max' => 64],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Group ID'),
            'group_name' => Yii::t('app', 'Group Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContexts()
    {
        return $this->hasMany(SipContext::className(), ['id' => 'sip_context_id'])->viaTable('sip_context_sip_group', ['sip_group_id' => 'id']);
    }



//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getSipUserGroups()
//    {
//        return $this->hasMany(SipUserGroup::className(), ['group_id' => 'id']);
//    }
//
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('sip_user_group', ['group_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
