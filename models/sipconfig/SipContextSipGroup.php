<?php

namespace app\models\sipconfig;

use Yii;

/**
 * This is the model class for table "sip_context_sip_group".
 *
 * @property integer $sip_context_id
 * @property integer $sip_group_id
 *
 * @property SipGroup $sipGroup
 * @property SipContext $sipContext
 */
class SipContextSipGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_context_sip_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sip_context_id', 'sip_group_id'], 'required'],
            [['sip_context_id', 'sip_group_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sip_context_id' => Yii::t('app', 'Sip Context ID'),
            'sip_group_id' => Yii::t('app', 'Sip Group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipGroup()
    {
        return $this->hasOne(SipGroup::className(), ['id' => 'sip_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipContext()
    {
        return $this->hasOne(SipContext::className(), ['id' => 'sip_context_id']);
    }
}
