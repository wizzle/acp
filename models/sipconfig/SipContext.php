<?php

namespace app\models\sipconfig;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sip_context".
 *
 * @property integer $id
 * @property string $context_name
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property SipContextSipGroup[] $sipContextSipGroups
 * @property SipGroup[] $sipGroups
 */
class SipContext extends ActiveRecord
{
    /** @property string $groups */
    protected $sipGroups;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_context';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['context_name'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['sipGroups'], 'safe'],
            [['context_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'context_name' => Yii::t('app', 'Context Name'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Returns all available group items to be attached to context.
     * @return array
     */
    public function getAvailableGroups()
    {
        return ArrayHelper::map(SipGroup::find()->all(), 'id', function ($item) {
            return empty($item->description)
                ? $item->group_name
                : $item->group_name . ' (' . $item->description . ')';
        });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipContextSipGroups()
    {
        return $this->hasMany(SipContextSipGroup::className(), ['sip_context_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipGroups()
    {
        return $this->hasMany(SipGroup::className(), ['id' => 'sip_group_id'])->viaTable('sip_context_sip_group', ['sip_context_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(is_array($this->sipGroups)){
            SipContextSipGroup::deleteAll(['sip_context_id' => $this->id]);
            $values = [];
            foreach ($this->sipGroups as $group_id) {
                $values[] = [$this->id, $group_id];
            }
            self::getDb()->createCommand()
                ->batchInsert(SipContextSipGroup::tableName(), ['sip_context_id', 'sip_group_id'], $values)->execute();
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
