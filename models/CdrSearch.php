<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CdrSearch extends Cdr
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['channel', 'dstchannel'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Cdr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['or like', 'channel', $this->channel,])
            ->andFilterWhere(['or like', 'dstchannel', $this->dstchannel]);

        return $dataProvider;
    }
}