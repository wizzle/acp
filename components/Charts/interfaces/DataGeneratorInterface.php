<?php

namespace app\components\Charts\interfaces;

/**
 * DataGeneratorInterface
 *
 * @author Dorofeev Maksim <makdorf@gmail.com>
 *
 */
interface DataGeneratorInterface
{
    /**
     * Returns the primary key **name(s)** for this AR class.
     *
     * Note that an array should be returned even when the record only has a single primary key.
     *
     * For the primary key **value** see [[getPrimaryKey()]] instead.
     *
     * @return string[] the primary key name(s) for this AR class.
     */
}