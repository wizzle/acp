<?php

namespace app\components\Charts\interfaces;

/**
 * DataRowInterface
 *
 * @author Dorofeev Maksim <makdorf@gmail.com>
 *
 */
interface DataRowInterface
{
    /**
     * Returns the array
     *
     * Note that an array should be returned even when the record only has a single primary key.
     *
     * For the primary key **value** see [[getPrimaryKey()]] instead.
     *
     * @return string[] the primary key name(s) for this AR class.
     */
    public function toArray();

    public function toString();

}