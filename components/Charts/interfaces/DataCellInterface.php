<?php

namespace app\components\Charts\interfaces;

/**
 * DataCellInterface represents a cell in a Google DataTable
 * Cell в понимании google это один из элементов графика.
 * Т.е. cell это координата по оси X например. Допустим
 * на оси X мы отмечаем года работы, а по оси Y будет отображать
 * уровень доходов. Таким образом если взять график за 2001 год,
 * то "2001" это уже cell. Уровень доходов который мы отображаем
 * над этой координатой тоже cell. Вместе они составляют Row.
 * Т.е. cell это составляюшая row, и вместе они представляют
 * элемент графика. Следом за "2001" следует "2002", он, в свою
 * очередь, тоже состоит из cell. И т.д.
 *
 * Cell Objects
 *
 * Each cell in the table is described by an object with the following properties:
 *
 * v [Optional] The cell value. The data type should match the column data type.
 * If the cell is null, the v property should be null,
 * though it can still have f and p properties.
 *
 * f [Optional] A string version of the v value, formatted for display.
 * Typically the values will match, though they do not need to,
 * so if you specify Date(2008, 0, 1) for v, you should specify "January 1, 2008"
 * or some such string for this property. This value is not checked against
 * the v value. The visualization will not use this value for calculation,
 * only as a label for display. If omitted, a string version
 * of v will be automatically generated using the default formatter.
 * The f values can be modified using your own formatter,
 * or set with setFormattedValue() or setCell(), or retrieved with getFormattedValue().
 *
 * p [Optional] An object that is a map of custom values applied to the cell.
 * These values can be of any JavaScript type. If your visualization supports
 * any cell-level properties, it will describe them. These properties can be
 * retrieved by the getProperty() and getProperties() methods.
 * Example: p:{style: 'border: 1px solid green;'}.
 *
 * Cells in the row array should be in the same order as their column descriptions in cols.
 * To indicate a null cell, you can specify null, leave a blank for
 * a cell in an array, or omit trailing array members.
 * So, to indicate a row with null for the first two cells,
 * you could specify [ , , {cell_val}] or [null, null, {cell_val}].
 *
 *
 * @link https://developers.google.com/chart/interactive/docs/reference?csw=1#DataTable
 * @link https://developers.google.com/chart/interactive/docs/reference?csw=1#DataTable_toJSON
 * @author Dorofeev Maksim <makdorf@gmail.com>
 *
 */
interface DataCellInterface
{
    public function toArray();

    public function toString();
}