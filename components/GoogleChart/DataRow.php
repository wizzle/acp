<?php

namespace app\components\GoogleChart;


use app\components\Charts\interfaces\DataRowInterface;

class DataRow implements DataRowInterface
{
    private $cells = [];

    public function __construct(array $cells)
    {
        foreach($cells as $key => $cell) {
            if (is_array($cell)) {
                $v = isset($cell['v']) ? $cell['v'] : null;
                $f = isset($cell['f']) ? $cell['f'] : null;
                $p = isset($cell['p']) ? $cell['p'] : null;
                $this->cells[$key] = new DataCell($v, $f, $p);
            } else {
                $this->cells[] = new DataCell($cell);
            }
        }
    }

    public function toArray()
    {
        $arr = [];
        foreach ($this->cells as $cell) {
            $arr[] = $cell->toArray();
        }
        return $arr;
    }

    public function toString()
    {
        $arr = [];
        foreach ($this->cells as $cell) {
            $arr[] = $cell->toString();
        }
        return $arr;
    }
}