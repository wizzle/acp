<?php

namespace app\components\GoogleChart;

use app\components\Charts\interfaces\DataGeneratorInterface;
use yii\base\Component;
use yii\base\ErrorException;
use yii\helpers\VarDumper;

class DataGenerator extends Component implements DataGeneratorInterface
{
    private $columns = [];
    private $rows = [];

    /**
     * Метод добавляет столбец.
     * @param $label
     * @param $type
     * @param null $role
     */
    public function addColumn($label, $type, $role = null)
    {
        $this->columns[] = new DataColumn($label, $type, $role);
    }

    /**
     * Метод добавляет строку.
     * @param array $row
     * Массив может быть как простым индексированным массивом ['a', 'b', 'c']
     * Так и многомерным массивом вида:
     * [
     *      [
     *          'v' => 'value',
     *          'f' => 'format',
     *          'p' => ['array of options'], https://developers.google.com/chart/interactive/docs/reference#methods (search on page 'rows property')
     *      ],
     *      [...],
     * ]
     * @throws ErrorException
     * @return bool
     */
    public function addRow(array $row)
    {
        if ($this->isValidRow($row)) {
            $this->rows[] = new DataRow($row);
            return true;
        }
        throw new ErrorException('Count of row elements must be equally ' . count($this->getColsArray()) . '. You have ' . count($row));
    }



    public function getColumns()
    {
        return $this->getColsArray();
    }

    public function getRows()
    {
        return $this->getRowsArray();
    }

    /**
     * Return an array representation of the DataTable (to JSON)
     * It doesn't try to match the column ids to data ids
     * so all the dataRows cell needs to be defined (put null if you need)
     * @return array
     */
    public function toArray()
    {
        $colsArray = $this->getColsLabel();
        $rowsArray = $this->getRowLabels();

        return array_merge($colsArray, $rowsArray);

    }

    public function toJson()
    {
        $colsArray = $this->getColsArray();
        $rowsArray = $this->getRowsArray();

        return json_encode([
            'cols' => $colsArray,
            'rows' => $rowsArray,
        ]);
    }

    private function isValidRow($row)
    {
        return count($this->getColsArray()) === count($row);
    }

    private function getRowsArray()
    {
        $rowsArray = $this->rows;
        array_walk($rowsArray, function (&$item) {
            /**
             * @var $item DataRow
             */
            $item = ['c' => $item->toArray()];
        });
        return $rowsArray;
    }

    private function getColsArray()
    {
        $colsArray = $this->columns;
        array_walk($colsArray, function (&$item) {
            /**
             * @var $item DataColumn
             */
            $item = $item->toArray();
        });

        return $colsArray;
    }

    private function getColsLabel()
    {
        $colsArray = $this->columns;
        array_walk($colsArray, function (&$item) {
            /**
             * @var $item DataColumn
             */
            $item = $item->isRole() ? ['role' => $item->getRole()] : $item->getLabel();
        });

        return [$colsArray];
    }

    private function getRowLabels()
    {
        $rowsArray = $this->rows;
        array_walk($rowsArray, function (&$item) {
            /**
             * @var $item DataRow
             */
            $item = $item->toString();
        });

        return $rowsArray;
    }
}