<?php

namespace app\components\GoogleChart;


/**
 * This class represents a cell in a Google DataTable
 * @link http://code.google.com/apis/chart/interactive/docs/reference.html#DataTable
 */
class DataCell
{
    private $v;
    private $f;
    private $p;

    public function __construct($v, $f = null, $p = null)
    {
        $this->v = $v;
        $this->f = $f;
        $this->p = $p;
    }

    /**
     *returns an array representation of the cell (useful for JSON)
     * @return array
     */
    public function toArray()
    {
        $arr = [
            'v' => $this->v,
            'f' => $this->f,
            'p' => $this->p,
        ];
        $arr = array_filter( $arr, function ($val) { return null !== $val; } );

        return $arr;
    }

    public function toString()
    {
        return $this->v;
    }

}