<?php

namespace app\components\GoogleChart;


use yii\base\Exception;
use yii\base\InvalidValueException;

class DataColumn
{
    // timeofday: an array of four numbers: [hour, minute, second, millisenconds].
    // date and datetime: a js Date object...
    public static $validTypes = ['string', 'number', 'date',  'datetime', 'timeofday', 'boolean'];

    /**
     * @var array
     * @link https://developers.google.com/chart/interactive/docs/roles#what-roles-are-available
     */
    public static $validRoles = [
        'annotation',
        'annotationText',
        'certainty',
        'emphasis',
        'interval',
        'scope',
        'style',
        'tooltip',
        'domain',
        'data',
    ];

//    protected $id;
    private $label;
    private $type;
    private $role;

    /**
     *
     * @param string $label
     * @param string $type one of self::$validTypes
     * @param string $role one of self::$validRoles
     * @throws InvalidValueException
     * @throws Exception
     */
    public function __construct($label = '', $type = '', $role = null)
    {
        try {
            $this->isValid($label, $type, $role);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->label = $label;
        $this->type = $type;
        $this->role = $role;
    }

    private function isValid($label, $type, $role)
    {
        if (!in_array($type, self::$validTypes, true)) {
            throw new InvalidValueException('Invalid type for DataColumn ' . $type);
        }
        if (!isset($label,$type)) {
            throw new Exception ('DataColumn constructor excepts a label and an type');
        }
        if (!in_array($role, self::$validRoles, true) and null !== $role) {
            throw new Exception ('Invalid Column Role: ' . $role);
        }
        return true;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getRole()
    {
        return $this->role;
    }

    /**
     * Returns the label of the column
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $cols = [
            'label' => $this->getLabel(),
            'type' => $this->getType(),
        ];
        return array_merge($cols, $this->getColumnRole());
    }

    /**
     * Sets role of the column
     * @return array
     */
    private function getColumnRole()
    {
        if (null === $this->role) {
            return [];
        } else {
            return [
                'p' => [
                    'role' => $this->role,
                ],
            ];
        }
    }

    public function isRole()
    {
        return null !== $this->role;
    }
}