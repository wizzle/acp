<?php

namespace app\controllers;

use app\models\records\Cdr;
use app\models\CdrProfile;
use app\models\records\CdrSearch;
use app\models\records\RecordInfo;
use app\models\sipconfig\SipGroup;
use app\models\User;
use Yii;
use yii\base\ExitException;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use dektrium\user\Finder;
use yii\base\Module as Module2;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class RecordsController extends Controller
{
    /** @var Finder */
    protected $finder;

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function getNavBar(CdrProfile $cdrProfile)
    {
        $menuItems[] =
            [
                'label'   => Yii::t('app', 'My Calls'),
                'url'     => ['/records/my-records'],
                'active' => strstr(Yii::$app->request->getUrl(), '/records/my-records'),
            ];
        if(Yii::$app->user->can('listenGroupCalls')) {
            $groups = ArrayHelper::map($cdrProfile->userGroups, 'id', 'group_name');
            foreach ($groups as $groupId => $groupName) {
                $menuItems[] = [
                    'label' => Yii::t('app', $groupName),
                    'url'     => ['/records/group?id=' . $groupId],
                    'active' => strstr(Yii::$app->request->getUrl(), '/records/group?id=' . $groupId),
                ];
            }
        }

        return $menuItems;
    }
    /**
     * Lists all Cdr models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect('records/my-records');
    }

    public function actionMyRecords()
    {
        $cdrProfile = $this->findModel(Yii::$app->user->id)->cdrProfile;
        /**
         * Set default values for current user
         * @link http://www.yiiframework.com/wiki/663/yii2-default-values-for-index-data-provider/
         */
        $searchModel = new CdrSearch([
            'calldate' => date('Y-m-d', time()),
            'channel' => $cdrProfile->sip_account,
            'dstchannel' => $cdrProfile->sip_account,
            'dcontext' => $cdrProfile->allContexts,

        ]);
        $searchModel->scenario = CdrSearch::SCENARIO_SEARCH_MULTIPLE_RECORDS;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('cdr_test', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'navBar' => $this->getNavBar($cdrProfile),
            'channel' => [],
        ]);
    }

    public function actionGroup($id)
    {
        if (Yii::$app->user->can('listenGroupCalls')) {
            $group = new SipGroup(['id' => $id]);
            $users = $group->users;
            $channel = [];
            $dcontext = [];
            foreach ($users as $user) {
                /**
                 * при формировании списка пользователей состоящих в группе
                 * исключаем из этого списка тех кто может слушать вызовы
                 * своей группы. Т.к. туда попадал как сам владелец аттрибута 'listenGroupCalls'
                 * так и владельцы из других отделов (групп).
                 */
                $userRoles = Yii::$app->authManager->getRolesByUser($user->id);
                if (!array_key_exists('listenGroupCalls', $userRoles))
                {
                    $channel[$user->cdrProfile->inner_phone] = $user->cdrProfile->sip_account;
                    $dcontext = array_unique(array_merge($dcontext, $user->cdrProfile->allContexts));
                }
            }
            $searchModel = new CdrSearch([
                'calldate' => date('Y-m-d', time()),
                'channel' => $channel,
                'dstchannel' => $channel,
                'dcontext' => $dcontext,

            ]);
            $searchModel->scenario = CdrSearch::SCENARIO_SEARCH_MULTIPLE_RECORDS;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $cdrProfile = $this->findModel(Yii::$app->user->id)->cdrProfile;
            return $this->render('cdr_test', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'navBar' => $this->getNavBar($cdrProfile),
                'channel' => array_flip($channel),
            ]);
        }
        throw new ForbiddenHttpException("Sorry, you don't have access to this action.");
    }

    public function actionDetails()
    {
        Url::remember('', 'actions-redirect');
        $model = new CdrSearch();
//        $model->scenario = CdrSearch::SCENARIO_SEARCH_RECORD_BY_ID;
        $dataProvider = $model->searchById(Yii::$app->request->get());
        $infoModel = $model->getRecordInfo();
        if ($infoModel === null) {
            $infoModel = Yii::createObject(RecordInfo::className());
            $infoModel->link('record', $dataProvider);
        }

        return $this->renderPartial('detailview/_cdr_detail',[
            'model' => $dataProvider,
            'infoModel' => $infoModel,
        ]);
    }

    public function actionDetailsUpdate($id)
    {
        $model = new CdrSearch(['acctid' => $id]);
        $infoModel = $model->getRecordInfo();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'id' => $id,
            'isUpdated' => $infoModel->load(Yii::$app->request->post()) && $infoModel->save(),
        ];
    }

    public function actionClientSearch()
    {
        if (Yii::$app->request->get('channel') === null) {
            throw new BadRequestHttpException("Required params is not found");
        }
        if (Yii::$app->request->get('phone') === null) {
            throw new BadRequestHttpException("Required params is not found");
        }
        $model = new CdrSearch();
        $model->scenario = CdrSearch::SCENARIO_SEARCH_SRC_OR_DST_BY_PHONE;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = $model->searchByPhone(Yii::$app->request->get());
//        $channel = Yii::$app->request->get('channel') ? Yii::$app->request->get('channel') : $this->findModel(Yii::$app->user->id)->cdrProfile->sip_account;
        //$query = CdrSearch::find()->select(['building_name'])->where(['like', 'building_name', $term])->limit('10')->column();
        //print_r($term); exit;
        //return $query;
        return $result;
    }


    public function actionDebug($date = null)
    {
        if (is_null($date) or !$this->validateDate($date, 'Y-m-d')) {
            $date = date('Y-m-d', time());
        }
//        $profile = User::findOne(Yii::$app->user->identity->id)->profile;
//        $cdrProfile = User::findOne(Yii::$app->user->identity->id)->cdrProfile;
//        var_dump($cdrProfile->sip_account);
//         die;

        $currentUser = Yii::$app->user;
        if ($currentUser->can('salesManager') && $currentUser->can('viewRecords')) {
            $sips = ArrayHelper::map(CdrProfile::find()->asArray()->all(), 'user_id', 'sip_account');
            $model = Cdr::getCallDetailRecordsForUser($sips, $date);
            $datesModel = Cdr::getDatesOfCalls($sips, $date);
            return $this->render('cdr',[
                'model' => $model,
                'datesModel' => $datesModel,
                'currentUrl' => true,

            ]);
        } elseif ($currentUser->can('salesManager')) {
            $cdrProfile = User::findOne(Yii::$app->user->identity->id)->cdrProfile;
            $model = Cdr::getCallDetailRecordsForUser($cdrProfile->sip_account, $date);
            $datesModel = Cdr::getDatesOfCalls($cdrProfile->sip_account, $date);
            return $this->render('cdr', [
                'model' => $model,
                'datesModel' => $datesModel,
            ]);
        }
        throw new ForbiddenHttpException("Forbidden access");
    }

    public function actionOperator($number = null, $date = null) {
        if (is_null($date) or !$this->validateDate($date, 'Y-m-d'))
            $date = date('Y-m-d', time());

        if (!isset($number))
            throw new BadRequestHttpException("Operator must be specified");

        $userRoleArray = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        $targetUserRoleArray = Yii::$app->authManager->getRolesByUser(CdrProfile::getUserIdByInnerPhone($number));
        if (count($userRoleArray) > 1 or count($targetUserRoleArray) > 1)
            throw new BadRequestHttpException("Current user have more than 1 role. This is bad!");

        $currentUserRoleObject = array_pop($userRoleArray);
        $targetUserRoleObject = array_pop($targetUserRoleArray);

        if ($currentUserRoleObject == $targetUserRoleObject and Yii::$app->user->can('viewRecords')) {
            $targetUserCdrProfile = User::findOne(CdrProfile::getUserIdByInnerPhone($number))->cdrProfile;
            $model = Cdr::getCallDetailRecordsForUser($targetUserCdrProfile->sip_account, $date);
            $datesModel = Cdr::getDatesOfCalls([$targetUserCdrProfile->sip_account], $date);
            return $this->render('cdr', [
                'model' => $model,
                'datesModel' => $datesModel,
                'currentUrl' => true,
            ]);
        }




//        var_dump($targetUserAssigments, $userAssigments);
//        var_dump(array_key_exists ($keys[0], $userAssigments));
//        $sips = ArrayHelper::map(CdrProfile::find()->asArray()->all(), 'user_id', 'sip_account');

        // беру все роли пользователя по номеру телефона
//        $usersWithRole = Yii::$app->authManager->getUserIdsByRole($targetUserRole);
//        $targetUserAssigments = Yii::$app->authManager->getPermissionsByUser(CdrProfile::getUserIdByInnerPhone($operator));
//        var_dump($userAssigments->name); exit;
        // если пользователя нет, то массив с правами пустой, бросаем исключение
        if (empty($targetUserAssigments)) throw new BadRequestHttpException("Operator with phone number {$number} not found");
        // сохраняю в массив все найденного роли пользователя
        $keys = array_keys($targetUserAssigments);
        $currentUser = Yii::$app->user->ca;
        //сравниваем права текущего пользователя с целевым, если права пересекаются
        if ($currentUser->can($keys[0]) and $currentUser->can('headSalesManager')) {
            echo '123123';
        }
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }




    public function actionViewRecords($innerPhone)
    {
        var_dump($innerPhone);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $user = $this->finder->findUserById($id);
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }

        return $user;
    }

    /**
     * Performs AJAX validation.
     *
     * @param array|Model $model
     *
     * @throws ExitException
     */
    protected function performAjaxValidation($model)
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                Yii::$app->end();
            }
        }
    }
}