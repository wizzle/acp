<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09.02.2016
 * Time: 22:20
 */

namespace app\controllers;

use yii\filters\AccessControl;
use dektrium\user\Finder;
use yii\base\Module as Module2;

use yii\web\Controller;

class ReportsController extends Controller
{

    /** @var Finder */
    protected $finder;

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}