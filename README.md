Asterisk Control Panel project
============================

Панель управления астериском. Предназначена для увеличения продуктивности менеджеров по продажам.
Из панели доступны:
 1. Прослушивание собственных записей.
 2. Прослушивание записей группы.
 3. Цветовая пометка записей
 4. Добавление примечаний к записям.

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Clone sources from git:
~~~
git clone https://wizzle@bitbucket.org/wizzle/acp.git
~~~

### Install via Composer:

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix) or:

~~~
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
~~~

You can then install dependencies using the following command:
~~~
composer global require "fxp/composer-asset-plugin:^1.2.0"
composer update
chmod +x yii
~~~

### Accept migrations:
~~~
./yii migrate/up --migrationPath=@yii/rbac/migrations
./yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
./yii migrate/up
~~~

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=cdr',
    'username' => 'asterisk',
    'password' => 'asterisk',
    'charset' => 'utf8',
];
```

### Fill contexts
For search all availible context in your CDR do:
~~~
./yii context/update
~~~

### Fill rbac
For allow to show some group reports and records create next permissions:
~~~
viewRecords
viewReports
~~~

For allow to show some group reports and records create next role:
~~~
headSalesManager
    child: viewRecords
    child: viewReports
~~~

