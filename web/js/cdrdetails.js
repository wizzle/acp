var CdrDetails = {
    init: function () {
        $("#cdr").jExpand();
    },
    deleteRow: function () {
        $(function(){
           $("tbody").find("tr:not(.odd)").hide(53).empty();
           //$(this).find("tr:not(.odd)").empty();
        });
    }
};

var UpdateDetails = {
    sendRequest: function (selector) {
        var acctid = selector.data('id');
        var url = '/records/details-update?id='+acctid;
        var form = selector.find('form').serialize();
        return $.post(url, form);
    }
};

$(document).ready(function () {
    CdrDetails.init();
    $('[data-toggle="tooltip"]').tooltip();
    $("body").on( 'click', 'button.submit',
        function() {
           var result = UpdateDetails.sendRequest($('td.active'));
            result.done(function (data) {
                if (data.isUpdated === true) {
                    CdrDetails.deleteRow();
                }
            });
        }
    );

});