(function($){
    $.fn.jExpand = function(){
        var element = "tbody";
        //$(element).find("tr:odd").addClass("odd");
        //$(element).find("tr:not(.odd)").hide();
        $(element).find("tr.odd").click(function () {
            //$(this).next("tr").toggle(); //original
            //functional added by Mohorev
            var tr = $(this).next("tr");
            //var ip = $(this).next("td#ip").text();
            var id= $(this).data("id");
            var url = "/records/details?search%5Bacctid%5D="+id;
            $(this).toggleClass('active');

            //console.log(ip);
            $.get(url, function (content) {
                //TODO при закрытии tr-ки очищать её содержимое, сайчас запрос отправляется два раза - при открытии
                // и закрытии
                tr.html(content).toggle(109)
                //при открытии нового элемента - прячет все открытые
                .siblings("tr:not(.odd)").hide(53).empty();
            });
        });
    }
})(jQuery);