<?php

/* @var $this yii\web\View */
/* @var $datesModel app\models\Cdr */
/* @var $searchModel app\models\CdrSearch */
/* @var $currentUrl bool */

use yii\helpers\Html;
use app\assets\jexpandAsset;
use app\models\CdrProfile;
use yii\helpers\ArrayHelper;
use app\assets\searchAsset;
use kartik\select2\Select2;

jexpandAsset::register($this);
searchAsset::register($this);
$this->title = 'Call Detail Records';
$this->params['breadcrumbs'][] = $this->title;
//var_dump(\yii\helpers\ArrayHelper::map(\app\models\CdrProfile::find()->asArray()->all(), 'inner_phone', 'sip_account')); exit;
?>
<div class="selectize-search">
<!--    <label class="control-label">Provinces</label>-->
    <?= Select2::widget([
        'name' => 'state_2',
        'value' => '',
        'data' => ['1', '2'],
        'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
    ]);
    ?>
</div>

<div class="row">
<!--<div class="call-detail-record">-->
    <?php \yii\widgets\Pjax::begin(['id' => 'pjax-container',]); ?>

    <div class="call-detail-record-search col-md-2">
        <?//= $this->render('_cdr_tree_view', [
//            'datesModel' => $datesModel,
//            'currentUrl' => $currentUrl,
//            ]) ?>


    </div>

    <div class="col-md-10">

        <?= \yii\grid\GridView::widget([
        'dataProvider' => $model,
//        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered table-hover table-condensed',
            'id' => 'cdr',
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['data-id' => $model->acctid, 'class' => 'odd'];
        },
        'afterRow' => function($model, $key, $index) {
            return
                Html::tag('tr',
                    Html::tag('td',
                        Html::tag('span',''),
                    ['colspan'=>'5']),
                ['style' => 'display: none;']
            );
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], //отображает номера строк
            'calldate',
            [
                'class' => \yii\grid\DataColumn::className(), // Не обязательно
                'attribute' => function($model) {
                    Yii::$app->formatter->timeZone = 'UTC';
                    return Yii::$app->formatter->asTime($model->billsec, 'php:H:i:s');
                },
                'label' => Yii::t('app', 'Duration'),
            ],
            [
                'class' => \yii\grid\DataColumn::className(), // Не обязательно
                'attribute' => function($model) {
                    $in = (Html::tag('div',Html::tag('i', '', ['class' => 'fa fa-sign-in fa-2x']),['class' => 'icons']));
                    $out = (Html::tag('div',Html::tag('i', '', ['class' => 'fa fa-sign-out fa-2x']),['class' => 'icons']));
                    return (strlen($model->src) > 4) ? $in : $out;
                },
                'format' => 'html',
                'label' => 'Type',
            ],
            [
                'class' => \yii\grid\DataColumn::className(), // Не обязательно
                'attribute' => function($model) {
                    //https://github.com/giggsey/libphonenumber-for-php
                    $phoneNumber = strlen($model->src) > 4 ? $model->src : $model->dst;
                    $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
                    try {
                        $phoneNumberObject = $phoneUtil->parse($phoneNumber, "BY");
                        return $phoneUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::NATIONAL);
                    } catch (\libphonenumber\NumberParseException $e) {
                        return $phoneNumber;
                    }
                },
                'format' => 'html',
                'label' => 'Number',
            ],
            [
//                'class' => \yii\grid\DataColumn::className(), // Не обязательно
                'attribute' => function($model) {
                    //TODO это же ведь костыль, поэтому надо подумать как выбирать лучше, например по dstchannel делать кросс запрос в базу, и от туда доставать полное имя специалиста.
                    $operator = strlen($model->src) > 4 ? substr($model->dstchannel, 4, 3) : $model->src;
                    $operator = (Html::tag('a',$operator,['href'=> \yii\helpers\Url::to(['records/operator', 'number' => $operator, 'date' => Yii::$app->request->get('date')]), 'data-pjax' => 0, 'target' => '_blank']));
                    return $operator;
                },
                'format' => 'html',
                'label' => 'Operator',
//                'filter' => Html::activeListBox($searchModel, 'channel', ArrayHelper::map(CdrProfile::find()->asArray()->all(), 'sip_account', 'inner_phone')),
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>
