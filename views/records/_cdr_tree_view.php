<?php

use yii\widgets\Pjax;
use yii\web\JsExpression;
use execut\widget\TreeView;
use app\models\treeview\TreeViewGenerator;

$onSelect = new JsExpression(<<<JS
    function (undefined, item) {
        if (item.href !== location.pathname) {
            $.pjax({
                container: '#pjax-container',
                url: item.href,
                timeout: null
            });
        }

            var otherTreeWidgetEl = $('.treeview.small').not($(this)),
            otherTreeWidget = otherTreeWidgetEl.data('treeview'),
            selectedEl = otherTreeWidgetEl.find('.node-selected');
            if (selectedEl.length) {
                otherTreeWidget.unselectNode(Number(selectedEl.attr('data-nodeid')));
            }
    }
JS
);

$treeView = new TreeViewGenerator();
foreach ($datesModel as $model) {
    $treeView->addItem($model, $currentUrl);
}
$items = $treeView->getItems();

echo TreeView::widget([
'data' => $items,
'size' => TreeView::SIZE_MIDDLE,
'template' => TreeView::TEMPLATE_SIMPLE,
'clientOptions' => [
        'onNodeSelected' => $onSelect,
        'showTags' => true,
        'showBorder' => false,
    ],
]);