<?php

/* @var $model app\models\records\CdrOperatorSearch */
/* @var $channel array available channels for current user */

use app\assets\searchAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\jui\AutoComplete;
use kartik\select2\Select2;
use yii\web\JsExpression;

searchAsset::register($this);

?>

<div class="panel panel-default">
    <div class="panel-body">
        <?php
        $form = ActiveForm::begin([
            'method' => 'get',
            'action' => Url::toRoute(['', 'id' => Yii::$app->request->get('id')]),
            'fieldConfig' => [
                'options' => [
                    'class' => 'form-group form-group-sm',
                ],
            ],
        ]); ?>

        <?= $form->field($model, 'calldate')->widget(DatePicker::className(), [
            'dateFormat' => 'php:Y-m-d',
            'language' => 'ru',
            'clientOptions' => [
                'maxDate' => '+0D',
            ],
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Pick a preferred date',
            ],
        ]) ?>

        <?php if (Yii::$app->controller->route === 'records/group') : ?>
            <?= $form->field($model, 'channel')->widget(Select2::className(), [
                'data' => $channel,
                'theme' => 'bootstrap',
                'attribute' => 'channel',
                'maintainOrder' => true,
                'size' => 'sm',
                'options' => [
                    'placeholder' => 'Select operator ...',
                    'multiple' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'data' => true,
                ],
            ]) ?>
        <?php else: ?>
            <?= $form->field($model, 'channel')->hiddenInput()->label(false) ?>
        <?php endif; ?>

        <?= $form->field($model, 'phone')->widget(AutoComplete::className(), [
            'clientOptions' =>[
                'source' => New JsExpression("
                    function(request, response) {
                            var channel = $('#search-channel').val();
                            $.getJSON('".Url::toRoute(['/records/client-search'])."?channel=' + channel, {
                                phone: request.term
                            }, response);
                        }"
                ),
                'minLength' => '5',
            ],
            'options' => ['placeholder' => 'Enter client phone number', 'class' => 'form-control'],
        ]) ?>

        <?= \yii\helpers\Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm btn-block']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
