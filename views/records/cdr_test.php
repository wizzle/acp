<?php

/* @var $this yii\web\View */
/* @var $dataProvider app\models\records\Cdr */
/* @var $searchModel app\models\records\CdrOperatorSearch */
/* @var $navBar array menu items */
/* @var $channel array available channels for current user */
/* @var $currentUrl bool */

use yii\helpers\Html;
use app\assets\jexpandAsset;
use app\assets\spectrumPickerAsset;
spectrumPickerAsset::register($this);

jexpandAsset::register($this);
//if (!is_null($searchModel)) {
//    echo $this->render('_cdr_search_operator', ['model' => $searchModel]);
//}
//var_dump($cdrProfile->groups); exit;

?>
<?= $this->render('_menu', ['navbar' => $navBar]) ?>

<div class="row">
    <div class="col-lg-8 col-sm-2">
        <?php
        echo \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'summary' => false,
            'tableOptions' => [
                'class' => 'table table-bordered table-hover table-condensed',
                'id' => 'cdr',
            ],
            'rowOptions' => function ($model) {
                return ['data-id' => $model->acctid, 'class' => 'odd', 'style'=> 'background-color:'. $model->recordInfo['color'] .';'];
            },
            'afterRow' => function() {
                return
                    Html::tag('tr',
                        Html::tag('td',
                            Html::tag('span',Html::tag('td','',['colspan'=>'6'])),
                            ['colspan'=>'6']),
                        ['style' => 'display: none;']
                    );
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'], //отображает номера строк
                [
                    'attribute' => 'calldate',
                    'value' => 'calldate',
//                    'filter' => \yii\jui\DatePicker::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'calldate',
//                        'dateFormat' => 'php:Y-m-d',
//                        'language' => 'ru',
//                        'clientOptions' => [
//                            'maxDate' => '+0D'
//                        ],
//                        'options' => [
//                            'class' => 'form-control',
//                        ],
//                    ]),
                    'format' => 'html',
                ],
                'call_duration', // вычисляемое в mysql запросе поле, фактически в таблице его нет.
                [
                    'attribute' => function($model) {
                        $in = (Html::tag('div',Html::tag('i', '', ['class' => 'fa fa-sign-in fa-2x']),['class' => 'icons']));
                        $out = (Html::tag('div',Html::tag('i', '', ['class' => 'fa fa-sign-out fa-2x']),['class' => 'icons']));
                        return $model->direction === 'in' ? $in : $out; // вычисляемое в mysql запросе поле, фактически в таблице его нет.
                    },
                    'format' => 'html',
                    'label' => 'Type',
                    'enableSorting' => false,

                ],
                'clientPhone',
                [
                    'attribute' => function($model) {
                        //TODO это же ведь костыль, поэтому надо подумать как выбирать лучше, например по dstchannel делать кросс запрос в базу, и от туда доставать полное имя специалиста.
                        $operator = strlen($model->src) > 4 ? substr($model->dstchannel, 4, 3) : $model->src;
        //                $operator = (Html::tag('a',$operator,['href'=> \yii\helpers\Url::to(['records/operator', 'number' => $operator, 'date' => Yii::$app->request->get('date')]), 'data-pjax' => 0, 'target' => '_blank']));
                        return $operator;
                    },
                    'format' => 'html',
                    'label' => 'Operator',
                    'enableSorting' => false,
                    'visible' => Yii::$app->controller->route === 'records/group',
//                    'filter' => \kartik\select2\Select2::widget([
//                        'data' => $channel,
//                        'theme' => 'bootstrap',
//                        'attribute' => 'channel',
//                        'model' => $searchModel,
//                        'maintainOrder' => true,
//                        'size' => 'sm',
//                        'options' => [
//                            'placeholder' => 'Select operator ...',
//                            'multiple' => false,
//                        ],
//                        'pluginOptions' => [
//                            'allowClear' => true,
//                            'data' => true,
//                        ],
//                    ]),
                ],
            ],
        ]);
        ?>
    </div>
    <div class="col-lg-4 col-sm-2">
        <?= $this->render('_search', ['model' => $searchModel, 'channel' => $channel]) ?>
    </div>
</div>
