<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\records\CdrOperatorSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="post-search">
    <?php $form = ActiveForm::begin([
//        'action' => [''],
        'method' => 'get',
        'options' => [
        ],
    ]); ?>

    <?= $form->field($model, 'channel')->widget(\kartik\select2\Select2::classname(), [
        'data' => ['SIP/403' => '403', 'SIP/404' => '404', 'SIP/405' => '405'],
        'theme' => 'bootstrap',
        'maintainOrder' => true,
        'size' => 'sm',
        'options' => [
            'placeholder' => 'Select operator ...',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'data' => true,
        ],
        'addon' => [
            'prepend' => [
                'content' => Html::tag('span','',['class' => 'glyphicon glyphicon-earphone'])
            ],
            'append' => [
                'content' => Html::submitButton(Html::tag('span', '', ['class' => 'glyphicon glyphicon-search']),
                    [
                    'class' => 'btn btn-primary',
                    'title' => 'Search',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'right',
                    ]),
                'asButton' => true,
            ],
        ],
//        'pluginEvents' => [
//            "change" => "function() { log('change'); }",
//            "select2:opening" => "function() { log('select2:opening'); }",
//            "select2:open" => "function() { log('open'); }",
//            "select2:closing" => "function() { log('close'); }",
//            "select2:close" => "function() { log('close'); }",
//            "select2:selecting" => "function() { log('selecting'); }",
//            "select2:select" => "function() { log('select'); }",
//            "select2:unselecting" => "function() { log('unselecting'); }",
//            "select2:unselect" => "function() { log('unselect'); }"
//        ],
    ])->label(false); ?>
    <?php ActiveForm::end(); ?>
</div>