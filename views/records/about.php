<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>


    <?= \yii\grid\GridView::widget([
        'dataProvider' => $model->getCallDetailRecordsForUser(),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], //отображает номера строк
            'acctid',
            'src',
            'dst',
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => '{accept}',
//                'buttons' => [
//                    'accept' => function($url, $model) {
//                        $url = \yii\helpers\Url::toRoute(['/komplat/print', 'acctid' => $model->id]);
//                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-print"></span>', $url, [
//                            'title' => 'Распечатать договор',
//                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
//                        ]);
//                    }
//                ],
//            ],
        ],
    ]) ?>

</div>
