<?php
/* @var $infoModel app\models\records\CdrSearch */

$style = $infoModel->color ? 'style = "background-color: ' . $infoModel->color . ';"' : null;
?>

<td colspan='6' class='active' <?= $style ?> data-id ='<?= $model->acctid ?>'>
    <div class="row">
        <div class="col-lg-12">
            <div class='more-info'>
            <button type="button" class="close" aria-label="Close" onclick="CdrDetails.deleteRow();">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3><?= 'Call. Duration: ' . $model->call_duration ?></h3>
                <?= $this->render('_summary', ['model' => $model,]) ?>
                <div class="form-group">
                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                        'layout' => 'horizontal',
//                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-lg-9',
                            ],
                        ],
                    ]); ?>

                    <?= $form->field($infoModel, 'description')->textarea() ?>
                    <?= $form->field($infoModel, 'color')->input('text',['class' => 'colorPalette'])?>

                    <div class=" col-lg-12">
                    <?= \yii\helpers\Html::button(Yii::t('app', 'Update'), ['class' => 'btn btn-block btn-success submit']) ?>
                    <?php \yii\bootstrap\ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</td>
<script>
    function printColor(color) {
        var currentTd = $("td.active");
        currentTd.css('background-color',color.toHexString());
        var acctid = currentTd.data('id');
        $('tr[data-id='+acctid+']').css('background-color',color.toHexString()).toggleClass('active');
        $(".colorPalette").val(color.toHexString());
    }



    $(".colorPalette").spectrum({
        showPaletteOnly: true,
        hideAfterPaletteSelect: true,
        showPalette:true,
        color: '#f5f5f5',
        change: function(color) {
            printColor(color);
        },
        palette: [
            ["#dff0d8", "#d9edf7", "#fcf8e3", "#f2dede"]
        ]
    });
</script>

