<?php

/* @var $this yii\web\View */
/* @var $model app\models\records\CdrSearch */

use yii\helpers\Html;
use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'options' => [
        'class' => 'table table-bordered table-hover table-condensed',
    ],
    'attributes' => [
        'acctid',
        'calldate',
        [
            'attribute' => function($model) {
                $in = 'Incoming Call';
                $out = 'Outgoing Call';
                return $model->direction == 'in' ? $in : $out; // вычисляемое в mysql запросе поле, фактически в таблице его нет.
            },
            'format' => 'html',
            'label' => 'Type',
        ],
        'clientPhone',
        [
            'attribute' => function($model) {
                //TODO это же ведь костыль, поэтому надо подумать как выбирать лучше, например по dstchannel делать кросс запрос в базу, и от туда доставать полное имя специалиста.
                $operator = strlen($model->src) > 4 ? substr($model->dstchannel, 4, 3) : $model->src;
//                $operator = (Html::tag('a',$operator,['href'=> \yii\helpers\Url::to(['records/operator', 'number' => $operator, 'date' => Yii::$app->request->get('date')]), 'data-pjax' => 0, 'target' => '_blank']));
                return $operator;
            },
            'format' => 'html',
            'label' => 'Operator',
        ],
        [
            'label' => 'Listen',
            'format' => 'raw',
            'attribute' => function($model) {
                return "
                <div class='row'>
                <div class='col-xs-7'>
                    <audio preload='auto' controls>
                        <source src='".\yii\helpers\Url::to('/'.$model->recordfile)."' type='audio/mp3'>
                    </audio>
                </div>
                <div class='col-xs-1'>
                    <button type=\"button\" class=\"btn btn-default\" aria-label=\"Download\">
                        ".Html::a('<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>', \yii\helpers\Url::to('/'.$model->recordfile), ['download' => ''])."
                    </button>
                </div>
                ";
            }

            ,
        ],
    ],
]);
