<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Call Detail Records';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-detail-record">
    <h1><?= Html::encode($this->title) ?></h1>


    <?= \yii\grid\GridView::widget([
        'dataProvider' => $model,
        'tableOptions' => [
            'class' => 'table table-bordered table-hover'
        ],
        //http://stackoverflow.com/questions/28381221/gridview-row-as-link-except-action-column-items-in-yii2
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['data-id' => $model->acctid];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], //отображает номера строк
            'acctid',
            'src',
            'dst',
            'recordfile',
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => '{accept}',
//                'buttons' => [
//                    'accept' => function($url, $model) {
//                        $url = \yii\helpers\Url::toRoute(['/komplat/print', 'acctid' => $model->id]);
//                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-print"></span>', $url, [
//                            'title' => 'Распечатать договор',
//                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
//                        ]);
//                    }
//                ],
//            ],
        ],
    ]) ?>

    <?php
    $this->registerJs("

    $('td').click(function (e) {
        var id = $(this).closest('tr').data('id');
        if(e.target == this)
            location.href = '" . Yii::$app->urlManager->createUrl('accountinfo/update') . "?id=' + id;
    });

");?>

<!--    --><?//= DetailView::widget([
//        'model' => $model,
//        'attributes' => [
//            'title',               // title свойство (обычный текст)
//            'description:html',    // description свойство, как HTML
//            [                      // name свойство зависимой модели owner
//               'label' => 'Cdr',
//                'value' => '123',
//            ],
//            'created_at:datetime', // дата создания в формате datetime
//        ],
//    ]);
//    ?>

</div>
