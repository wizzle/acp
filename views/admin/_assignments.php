<?php


use dektrium\rbac\widgets\Assignments;

/**
 * @var yii\web\View 				$this
 * @var dektrium\user\models\User 	$user
 */

?>

<?php $this->beginContent('@app/views/admin/update.php', ['user' => $user]) ?>

<?= yii\bootstrap\Alert::widget([
    'options' => [
        'class' => 'alert-info',
    ],
    'body' => Yii::t('user', 'You can assign multiple roles or permissions to user by using the form below'),
]) ?>

<?= Assignments::widget(['userId' => $user->id]) ?>

<?php $this->endContent() ?>
