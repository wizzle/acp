<?php


use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View 					$this
 * @var dektrium\user\models\User 		$user
 * @var app\models\CdrProfile 	        $cdrProfile
 * @var array                           $sipGroups
 */
?>

<?php $this->beginContent('@app/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<?= $form->field($cdrProfile, 'inner_phone') ?>
<?= $form->field($cdrProfile, 'sip_account') ?>
<?= $form->field($cdrProfile, 'userGroups')->widget(\kartik\select2\Select2::classname(), [
    'data' => $cdrProfile->getAvailableGroups(),
    'theme' => 'bootstrap',
    'maintainOrder' => true,
    'options' => [
        'placeholder' => 'Select group ...',
        'multiple' => true,
        'name' => 'CdrProfile[groups][]', //TODO проблема, смотри коммент в модели CdrProfile
    ],
]); ?>
<div class="form-group context-badges">
    <label class="control-label col-sm-3" for="cdrprofile-groups">Availible Contexts</label>
    <div class="col-sm-9">
        <h5>
        <?php foreach( $cdrProfile->allContexts as $context ) : ?>
            <span class="label label-info"><?= $context ?></span>
        <?php endforeach ;?>
        </h5>
        <div class="help-block help-block-error "></div>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
