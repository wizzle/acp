<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sip Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('/sip/_menu') ?>
<div class="sip-group-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'group_name',
            [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'description',
                'enableSorting' => false,
            ],
            [
                'class' => \yii\grid\DataColumn::className(),
                'label' => 'Users in this group',
                'format' => 'text',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
