<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\sipconfig\SipGroup */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sip Group',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sip-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
