<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ACP',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Dashboard',
                'url' => ['/dashboard'],
//                'visible' => !Yii::$app->user->isGuest,
                'visible' => false,
            ],
            [
                'label' => 'Records',
                'items' => [
                    [
                        'label' => 'My Records',
                        'url' => ['/records'],
                    ],
                    [
                        'label' => 'Group Records',
                        'url' => ['/records/group'],
                        //'viewRecords' rbac permission
                        //http://local.host/rbac/permission/indexall
                        'visible' => Yii::$app->user->can('viewRecords'),
                    ],
                ],
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'Reports',
                'items' => [
                    [
                        'label' => 'My Reports',
                        'url' => ['/reports/charts'],
                    ],
                    [
                        'label' => 'Group Reports',
                        'url' => ['/reports'],
                        //'viewRecords' rbac permission
                        //http://local.host/rbac/permission/indexall
                        'visible' => Yii::$app->user->can('viewReports'),
                    ],
                ],
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'Admin Panel',
                'items' =>[
                    '<li class="dropdown-header">Users settings</li>',
                    [
                        'label' => 'Users',
                        'url' => ['/admin'],
                    ],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">CDR settings</li>',
                    [
                        'label' => 'Sip Groups',
                        'url' => ['/sip-group'],
                    ],
                    [
                        'label' => 'Sip Contexts',
                        'url' => ['/sip-context'],
                    ],
                ],
                'visible' => !Yii::$app->user->isGuest and Yii::$app->user->identity->getIsAdmin(),
            ],
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/user/security/login']] :
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/user/security/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ACP <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
