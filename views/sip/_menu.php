<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

?>

<?= Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
    ],
    'items' => [
        [
            'label'   => Yii::t('app', 'Sip Groups'),
            'url'     => ['/sip-group/index'],
        ],
        [
            'label'   => Yii::t('app', 'Sip Contexts'),
            'url'     => ['/sip-context/index'],
        ],
        [
            'label' => Yii::t('user', 'Create'),
            'items' => [
                [
                    'label'   => Yii::t('app', 'New Group'),
                    'url'     => ['/sip-group/create'],
                ],
                [
                    'label' => Yii::t('app', 'New Context'),
                    'url'   => ['/sip-context/create'],
                ],
            ],
        ],
    ],
]) ?>
