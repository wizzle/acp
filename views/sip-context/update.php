<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\sipconfig\SipContext */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sip Context',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Contexts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sip-context-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
