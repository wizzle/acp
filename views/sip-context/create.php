<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\sipconfig\SipContext */

$this->title = Yii::t('app', 'Create Sip Context');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Contexts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sip-context-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
