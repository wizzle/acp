<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\sipconfig\SipContext */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Contexts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sip-context-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'context_name',
            'description:ntext',
            [
                'label' => 'Users groups',
                'attribute' => function ($model) {
                    $string = '';
                    foreach ($model->sipGroups as $sipGroup) {
                        $string .= $sipGroup->group_name .', ';
                    }
                    return $string;
                },
            ],
        ],
    ]) ?>

</div>
