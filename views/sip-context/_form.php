<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\sipconfig\SipContext */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sip-context-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'context_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sipGroups')->widget(\kartik\select2\Select2::classname(), [
        'data' => $model->getAvailableGroups(),
        'theme' => 'bootstrap',
        'maintainOrder' => true,
        'options' => [
            'placeholder' => 'Select group ...',
            'multiple' => true,
        ],
//    'pluginOptions' => [
//        'allowClear' => true,
//        'data' => true,
//    ],

    ]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
