<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sip Contexts');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('/sip/_menu') ?>
<div class="sip-context-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'context_name',
            'description:ntext',
            [
                'label' => 'Users groups',
                'attribute' => function ($model) {
                    $string = '';
                    foreach ($model->sipGroups as $sipGroup) {
                        $string .= $sipGroup->group_name .', ';
                    }
                    return $string;
                },
                'enableSorting' => false,
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
