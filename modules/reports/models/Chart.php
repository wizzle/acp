<?php

namespace app\modules\reports\models;


use app\components\GoogleChart\DataGenerator;
use app\modules\reports\helpers\ChartHelper;
use yii\base\ErrorException;

class Chart
{
    private $chartData = [];


    public function __construct(array $chartData)
    {
        if (0 !== count($chartData)) {
            $this->chartData = ChartHelper::init($chartData, \Yii::$app->user->id);
        }
    }

    public function getDurationColumns()
    {
        $chart = new DataGenerator();
        $chart->addColumn('Оператор', 'string');
        $chart->addColumn('Входящие вызовы', 'number');
        $chart->addColumn('Исходящие вызовы', 'number');
        $chart->addColumn('annotation', 'string', 'annotation');
        $chart->addColumn('opacity', 'string', 'style');

        try {
            $chart->addRow(ChartHelper::getDurationColumnRow(ChartHelper::getLeaderData()));
            $chart->addRow(ChartHelper::getDurationColumnRow(ChartHelper::getSecondData()));
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
        return $chart->toArray();
    }
    
    public function getDurationPie()
    {
        $companyDataPie = [
            ChartHelper::getCompanyLabel(),
            round(ChartHelper::getSum(CdrAnsweredOnlySearch::$totalDurationColumnAlias)/60/60, 2),
        ];

        $chart = new DataGenerator();
        $chart->addColumn('Оператор', 'string'); 
        $chart->addColumn('Время, час.', 'string');

        try {
            $chart->addRow(ChartHelper::getDurationPieRow(ChartHelper::getCurrentUserData()));
            $chart->addRow($companyDataPie);
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
        return $chart->toArray();
    }

    public function getCountColumns()
    {
        $chart = new DataGenerator();
        $chart->addColumn('Оператор', 'string');
        $chart->addColumn('Входящие вызовы', 'number');
        $chart->addColumn('Исходящие вызовы', 'number');
        $chart->addColumn('annotation', 'string', 'annotation');

        try {
            $chart->addRow(ChartHelper::getCountColumnRow(ChartHelper::getLeaderData()));
            $chart->addRow(ChartHelper::getCountColumnRow(ChartHelper::getSecondData()));
        } catch (ErrorException $e) {
            echo $e->getMessage(); //ok
        }
        return $chart->toArray();
    }

    public function getCountPie()
    {
        $companyDataPie = [
            ChartHelper::getCompanyLabel(),
            ChartHelper::getSum(CdrAnsweredOnlySearch::$totalCountColumnAlias),
        ];

        $chart = new DataGenerator();
        $chart->addColumn('Оператор', 'string');
        $chart->addColumn('Количество, шт.', 'string');
        try {
            $chart->addRow(ChartHelper::getCountPieRow(ChartHelper::getCurrentUserData()));
            $chart->addRow($companyDataPie);
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }

        return $chart->toArray();
    }
}