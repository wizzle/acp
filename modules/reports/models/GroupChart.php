<?php

namespace app\modules\reports\models;


use app\components\GoogleChart\DataGenerator;
//use app\modules\reports\helpers\ChartHelper;
use app\modules\reports\helpers\GroupChartHelper;
use yii\base\ErrorException;
use yii\helpers\VarDumper;

class GroupChart
{
    private $chartData = [];
    private $durationColumnsTitle = [
        ['Оператор', 'string'],
        ['Входящие вызовы', 'number'],
        ['Исходящие вызовы', 'number'],
        ['annotation', 'string', 'annotation'],
        ['opacity', 'string', 'style'],

    ];

    public function __construct(array $chartData)
    {
//        VarDumper::dump($chartData,10,true);
//        VarDumper::dump(GroupChartHelper::init($chartData),10,true);

//        exit;

        if (0 !== count($chartData)) {
            $this->chartData = $chartData;
        }
    }

    public function getDurationColumns()
    {
        $chart = new DataGenerator();

        foreach ($this->durationColumnsTitle as $col) {
            $chart->addColumn(...$col); //http://php.net/manual/ru/functions.arguments.php#functions.variable-arg-list
        }

        try {
            foreach ($this->chartData as $data) {
                $chart->addRow(GroupChartHelper::getDurationColumnRow($data));
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }

        return $chart;
    }

    public function getCountColumns()
    {
        $chart = new DataGenerator();
        $chart->addColumn('Оператор', 'string');
        $chart->addColumn('Входящие вызовы', 'number');
        $chart->addColumn('Исходящие вызовы', 'number');
        $chart->addColumn('annotation', 'string', 'annotation');

        try {
            foreach ($this->chartData as $data) {
                $chart->addRow(GroupChartHelper::getCountColumnRow($data));
            }
        } catch (ErrorException $e) {
            echo $e->getMessage(); //ok
        }
        return $chart;
    }











































//    public function getDurationPie()
//    {
//        $companyDataPie = [
//            GroupChartHelper::getCompanyLabel(),
//            round(GroupChartHelper::getSum(CdrAnsweredOnlySearch::$totalDurationColumnAlias)/60/60, 2),
//        ];
//
//        $chart = new DataGenerator();
//        $chart->addColumn('Оператор', 'string');
//        $chart->addColumn('Время, час.', 'string');
//
//        try {
//            foreach ($this->chartData as $data) {
//                $chart->addRow(GroupChartHelper::getDurationColumnRow($data));
//            }
//        } catch (ErrorException $e) {
//            echo $e->getMessage();
//        }
//        return $chart->toArray();
//    }



//    public function getCountPie()
//    {
//        $companyDataPie = [
//            GroupChartHelper::getCompanyLabel(),
//            GroupChartHelper::getSum(CdrAnsweredOnlySearch::$totalCountColumnAlias),
//        ];
//
//        $chart = new DataGenerator();
//        $chart->addColumn('Оператор', 'string');
//        $chart->addColumn('Количество, шт.', 'string');
//        try {
//            $chart->addRow(GroupChartHelper::getCountPieRow(GroupChartHelper::getCurrentUserData()));
//            $chart->addRow($companyDataPie);
//        } catch (ErrorException $e) {
//            echo $e->getMessage();
//        }
//
//        return $chart->toArray();
//    }
}