<?php

namespace app\modules\reports\models;

use yii;

/**
 * This is the model class for table "cdr_answered_only".
 *
 * @property integer $id
 * @property integer $cdr_acctid
 * @property integer $call_date
 * @property integer $duration
 * @property string $type
 * @property string $dcontext
 * @property string $client_phone
 * @property integer $operator_id
 * @property string $sip_account
 * @property string $record_file_path
 * @property integer $import
 */
class CdrAnsweredOnly extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cdr_answered_only';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cdr_acctid', 'call_date', 'duration'], 'required'],
            [['cdr_acctid', 'call_date', 'duration', 'operator_id', 'import'], 'integer'],
            [['type'], 'string'],
            [['dcontext', 'record_file_path'], 'string', 'max' => 255],
            [['client_phone'], 'string', 'max' => 14],
            [['sip_account'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cdr_acctid' => Yii::t('app', 'Cdr Acctid'),
            'call_date' => Yii::t('app', 'Call Date'),
            'duration' => Yii::t('app', 'Duration'),
            'type' => Yii::t('app', 'Type'),
            'dcontext' => Yii::t('app', 'Dcontext'),
            'client_phone' => Yii::t('app', 'Client Phone'),
            'operator_id' => Yii::t('app', 'Operator ID'),
            'sip_account' => Yii::t('app', 'Sip Account'),
            'record_file_path' => Yii::t('app', 'Record File Path'),
            'import' => Yii::t('app', 'Import'),
        ];
    }
}
