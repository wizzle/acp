<?php

namespace app\modules\reports\models;

use yii;
use yii\db\Query;
use yii\db\Expression;

/**
 * This is the search model class for table "cdr_answered_only".
 */
class CdrAnsweredOnlySearch extends CdrAnsweredOnly
{
    public static $durationIncomingAlias = 'duration_incoming';
    public static $durationOutgoingAlias = 'duration_outgoing';
    public static $totalDurationColumnAlias = 'duration_total';
    public static $countIncomingAlias = 'count_incoming';
    public static $countOutgoingAlias = 'count_outgoing';
    public static $totalCountColumnAlias = 'count_total';
    public static $averageIncomingTime = 'average_incoming_time';
    public static $averageOutgoingTime = 'average_outgoing_time';


    public $searchStartDate;
    public $searchEndDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['searchStartDate', 'searchEndDate'], 'date', 'format' => 'Y-m-d'],
            ['call_date', 'string'],
//            [['cdr_acctid', 'call_date', 'duration'], 'required'],
//            [['cdr_acctid', 'call_date', 'duration', 'operator_id', 'import'], 'integer'],
//            [['type'], 'string'],
//            [['dcontext', 'record_file_path'], 'string', 'max' => 255],
//            [['client_phone'], 'string', 'max' => 14],
//            [['sip_account'], 'string', 'max' => 20],
        ];
    }

    public function search($params)
    {
        /**
         * SELECT operator_id, sip_account,
        SUM(IF(`type` = 1, duration,0)) as duration_incoming,
        SUM(IF(`type` = 2, duration,0)) as duration_outgoing,
        (SUM(IF(`type` = 1, duration,0)) + SUM(IF(`type` = 2, duration,0))) as Total_Duration,
        SUM(IF(`type` = 1, 1,0)) as count_incoming,
        SUM(IF(`type` = 2, 1,0)) as count_outgoing,
        (SUM(IF(`type` = 1,1,0)) + SUM(IF(`type` = 2,1,0))) as Total_Count,
        FLOOR(SUM(IF(`type` = 1, duration,0))/SUM(IF(`type` = 1, 1,0))) as average_incoming_time,
        FLOOR(SUM(IF(`type` = 2, duration,0))/SUM(IF(`type` = 2, 1,0))) as average_outgoing_time
        from `cdr_answered_only` WHERE (operator_id IN (24, 23, 22, 21, 20)) and (call_date BETWEEN 1456779600 AND 1456866000) group by operator_id
         */
        $subQueryUserName = (new Query())
            ->select('name')
            ->from('profile')
            ->where(['user_id' => new Expression('cdr_answered_only.operator_id')]);
        $subQueryUserPhone = (new Query())
            ->select('inner_phone')
            ->from('cdr_profile')
            ->where(['user_id' => new Expression('cdr_answered_only.operator_id')]);
        $query = self::find()->select([
            'operator_id',
            'sip_account',
            'user_name' => $subQueryUserName,
            'inner_phone' => $subQueryUserPhone,
            'SUM(IF(`type` = 1, duration, 0)) as ' . self::$durationIncomingAlias,
            'SUM(IF(`type` = 2, duration, 0)) as ' . self::$durationOutgoingAlias,
            '(SUM(IF(`type` = 1, duration, 0)) + SUM(IF(`type` = 2, duration, 0))) as ' . self::$totalDurationColumnAlias,
            'SUM(IF(`type` = 1, 1, 0)) as ' . self::$countIncomingAlias,
            'SUM(IF(`type` = 2, 1, 0)) as ' . self::$countOutgoingAlias,
            '(SUM(IF(`type` = 1, 1, 0)) + SUM(IF(`type` = 2, 1, 0))) as ' . self::$totalCountColumnAlias,
            'FLOOR(SUM(IF(`type` = 1, duration, 0))/SUM(IF(`type` = 1, 1, 0))) as ' . self::$averageIncomingTime,
            'FLOOR(SUM(IF(`type` = 2, duration, 0))/SUM(IF(`type` = 2, 1, 0))) as ' . self::$averageOutgoingTime,
        ])
            ->where(['in', 'operator_id', $this->operator_id])
            ->groupBy(['operator_id']);

        $this->load($params);
        if ( null !== $this->call_date && strpos($this->call_date, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->call_date);
            $query->andFilterWhere(['between', 'call_date', strtotime($start_date), strtotime($end_date . ' +1day')]);
        } else {
            $query->andWhere(['between', 'call_date', strtotime($this->searchStartDate), strtotime($this->searchEndDate . ' +1day')]);
        }
        //yii\helpers\VarDumper::dump($query->createCommand()->getRawSql(), 10, true);
        return $query->asArray()->all();
    }
}
