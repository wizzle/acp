<?php

namespace app\modules\reports\controllers;

use app\models\sipconfig\SipGroup;
use app\models\User;
use app\modules\reports\models\CdrAnsweredOnlySearch;
use app\modules\reports\models\Chart;
use app\modules\reports\models\GroupChart;
use yii\base\Module as Module2;
use dektrium\user\Finder;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Default controller for the `report` module
 */
class ChartsController extends Controller
{

    protected static $validChartTypes = [
        'duration',
        'count',
        'average',
    ];

    /** @var Finder */
    protected $finder;

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('charts/my-charts');
    }

    public function actionMyCharts()
    {

        $model = new CdrAnsweredOnlySearch([
            'operator_id' => $this->getUsersIdFromGroup(),
//            'searchStartDate' => strtotime('midnight'),
            'searchStartDate' => date('d-m-Y', time()),
//            'searchEndDate' => strtotime('tomorrow midnight'),
            'searchEndDate' => date('d-m-Y',time()),
        ]);
        $searchResult = $model->search(Yii::$app->request->queryParams);
        $chartModel = new Chart($searchResult);
        return $this->render('charts', [
            'model' => $model,
            'chartModel' => $chartModel,
        ]);
    }

    public function actionGroup($id)
    {
        $model = new CdrAnsweredOnlySearch([
            'operator_id' => $this->getUsersIdFromGroup(),
            'searchStartDate' => '20-08-2016',
            'searchEndDate' => '25-08-2016',
        ]);
        $group = new SipGroup(['id' => $id]);

        $searchResult = $model->search(Yii::$app->request->queryParams);
//        VarDumper::dump($this->getUsersIdFromGroup(),10,true);
//        VarDumper::dump($searchResult,10,true);
        $chartModel = new GroupChart($searchResult);
//        VarDumper::dump($chartModel->getDurationColumns(),10,true);
//
//        VarDumper::dump($chartModel->getDurationColumns(),10,true);
//        exit;
//        VarDumper::dump($this->getDurationColumns(),10,true);
//        exit;
        return $this->render('charts_group', [
            'model' => $model,
            'chartModel' => $chartModel,
        ]);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    private function getUsersIdFromGroup()
    {
        $cdrProfile = $this->findModel(Yii::$app->user->id)->cdrProfile;
        $groups = $cdrProfile->userGroups;
        $allUsers = [];
        foreach ($groups as $group) {
            $group = new SipGroup(['id' => $group->id]);
            $users = $group->users;
            $allUsers = ArrayHelper::merge(ArrayHelper::map($users, 'username', 'id'), $allUsers);
        }
        return $allUsers;
    }
    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $user = $this->finder->findUserById($id);
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }
        return $user;
    }
}
