<?php

namespace app\modules\reports\controllers;

use app\modules\reports\Module;
use yii\web\Controller;

/**
 * Default controller for the `report` module
 */
class ReportsController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

}
