<?php
/* @var $model app\modules\reports\models\Chart */

use scotthuangzl\googlechart\GoogleChart;

//var_dump($chartModel); exit;
?>
<div class="row">
    <div class="col-lg-9">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1>Длительность, <small>час.</small></h1>
                    <div class="col-lg-8">
                        <?= GoogleChart::widget([
                            'visualization' => 'ColumnChart',
                            'data' => $chartModel->getDurationColumns()->toArray(),
                            'options' => [
                                'legend' => [ 'position' => 'top', 'maxLines' => 3],
            				    'width' => 700,
                                'height' => 300,
                                'bar' => [
                                    'groupWidth' => 40
                                ],
                                'isStacked' => true,
                            ],
                        ])?>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1>Количество, <small>шт.</small></h1>
                    <div class="col-lg-8">
                        <?= GoogleChart::widget([
                            'visualization' => 'ColumnChart',
                            'data' => $chartModel->getCountColumns()->toArray(),
                            'options' => [
                                'legend' => [ 'position' => 'top', 'maxLines' => 3],
            				    'width' => 700,
                                'height' => 300,
                                'bar' => [
                                    'groupWidth' => 40
                                ],
                                'isStacked' => true,
                            ],
                        ])?>
                    </div>

                </div>
            </div>
            <p class="text-muted">
                Графики отображают информацию на текущий момент.
                График "столбики" включает в себя показатели по лидеру и мои собственные.
                График "пирог" включает в себя показатели по компании в целом и мои собственные.
            </p>
        </div>
    </div>
    <div class="col-lg-3 col-sm-2">
        <?= $this->render('_search', ['model' => $model]) ?>
    </div>
</div>