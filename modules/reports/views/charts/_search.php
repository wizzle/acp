<?php

/* @var $model app\models\records\CdrOperatorSearch */
/* @var $channel array available channels for current user */

use app\assets\searchAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
//use yii\jui\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\jui\AutoComplete;
use kartik\select2\Select2;
use yii\web\JsExpression;

searchAsset::register($this);

?>

<div class="panel panel-default">
    <div class="panel-body">
        <?php
        $form = ActiveForm::begin([
            'method' => 'get',
            'action' => Url::toRoute(['', 'id' => Yii::$app->request->get('id')]),
            'fieldConfig' => [
                'options' => [
                    'class' => 'form-group form-group-sm',
                ],
            ],
        ]); ?>
        <?= $form->field($model, 'call_date')->widget(DateRangePicker::className(), [
            'presetDropdown'=>true,
            'hideInput'=>true,
//            'startAttribute' => 'searchStartDate',
//            'endAttribute' => 'searchEndDate',
            'language'=>'ru',

            'pluginOptions'=>[
                'locale'=>['format' => 'DD-MM-YYYY'],
                'opens'=>'left',
            ],


//            'dateFormat' => 'php:Y-m-d',
//            'language' => 'ru',
//            'clientOptions' => [
//                'maxDate' => '+0D',
//            ],
//            'options' => [
//                'class' => 'form-control',
//                'placeholder' => 'Pick a preferred date',
//            ],
        ]) ?>


        <?= \yii\helpers\Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm btn-block']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
