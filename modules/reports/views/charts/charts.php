<?php
/* @var $model app\modules\reports\models\Chart */

use scotthuangzl\googlechart\GoogleChart;

?>
<div class="row">
    <div class="col-lg-9">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1>Длительность, <small>час.</small></h1>
                    <div class="col-lg-8">
                        <?= GoogleChart::widget([
                            'visualization' => 'ColumnChart',
                            'data' => $chartModel->getDurationColumns(),
                            'options' => [
                                'legend' => [ 'position' => 'top', 'maxLines' => 3],
    //        				    'width' => 500,
                                'height' => 300,
                                'bar' => [
                                    'groupWidth' => 40
                                ],
                                'isStacked' => true,
                            ],
                        ])?>
                    </div>
                    <div class="col-lg-4">
                        <?= GoogleChart::widget([
                            'visualization' => 'PieChart',
                            'data' => $chartModel->getDurationPie(), //$model->getDurationDataForPieChart()
                            'options' => [
                                'legend' => [ 'position' => 'top', 'maxLines' => 3],
    //                            'title' => 'Мой вклад в общее дело.',
            //                    'width' => 1100,
                                'height' => 300,
                                'slices' => [['offset' => 0.2]],
                                'pieStartAngle' => 90
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1>Количество, <small>шт.</small></h1>
                    <div class="col-lg-8">
                        <?= GoogleChart::widget([
                            'visualization' => 'ColumnChart',
                            'data' => $chartModel->getCountColumns(),
                            'options' => [
                                'legend' => [ 'position' => 'top', 'maxLines' => 3],
    //        				    'width' => 100,
                                'height' => 300,
                                'bar' => [
                                    'groupWidth' => 40
                                ],
                                'isStacked' => true,
                            ],
                        ])?>
                    </div>
                    <div class="col-lg-4">
                        <?= GoogleChart::widget([
                            'visualization' => 'PieChart',
                            'data' => $chartModel->getCountPie(),
                            'options' => [
                                'legend' => [ 'position' => 'top', 'maxLines' => 3],

    //                            'title' => 'Мой вклад в общее дело.',
                                //                    'width' => 1100,
                                'height' => 300,
                                'slices' => [['offset' => 0.2]],
                                'pieStartAngle' => 90
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <p class="text-muted">
                Графики отображают информацию на текущий момент.
                График "столбики" включает в себя показатели по лидеру и мои собственные.
                График "пирог" включает в себя показатели по компании в целом и мои собственные.
            </p>
        </div>
    </div>
    <div class="col-lg-3 col-sm-2">
        <?= $this->render('_search', ['model' => $model]) ?>
    </div>
</div>