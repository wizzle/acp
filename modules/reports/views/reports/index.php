<?php
/* @var $this yii\web\View */
/* @var $datesModel app\models\Cdr */
/* @var $searchModel app\models\CdrSearch */
/* @var $currentUrl bool */

use yii\helpers\Html;
?>

<div class="charts-index">

    <div class="jumbotron">
        <h1>Charts!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <div class="thumbnail">
                    <?= Html::img('http://placehold.it/400x300') ?>
                    <div class="caption">
                        <?= Html::tag('h3', 'My personal Charts') ?>
                        <?= Html::tag('h4', 'some statistic') ?>
                        <p>Today calls count: 120</p>
                        <p>Today calls duration: 1 hour 12 minutes</p>
                        <?=
                        Html::a('Show data', '/reports/charts', [
                            'class' => 'btn btn-primary',
                            'role' => 'button',
                        ])
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="thumbnail">
                    <?= Html::img('http://placehold.it/400x300') ?>
                    <div class="caption">
                        <?= Html::tag('h3', 'First Department Charts') ?>
                        <?= Html::tag('h4', 'some statistic') ?>
                        <p>Today calls count: 370</p>
                        <p>Today calls duration: 4 hour 52 minutes</p>
                        <?=
                        Html::a('Show data', '/reports/charts/group?id=1', [
                            'class' => 'btn btn-primary',
                            'role' => 'button',
                        ])
                        ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

