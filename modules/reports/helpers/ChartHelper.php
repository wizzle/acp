<?php

namespace app\modules\reports\helpers;

use Yii;
use yii\helpers\BaseArrayHelper;

class ChartHelper extends BaseArrayHelper
{
    /**
     * @var array содержит массивы для построения графиков текущего пользователя и двух лидеров.
     */
    private static $topCallers = [
        'leader' => [],
        'second' => [],
        'current_user' => [],
    ];
    private static $rawData = [];
    private static $currentUserId;

    public static function getLeaderData()
    {
        return static::$topCallers['leader'];
    }

    public static function getSecondData()
    {
        if (static::$topCallers['leader'] === static::$topCallers['current_user']) {
            return static::$topCallers['second'];
        }

        return static::$topCallers['current_user'];
    }

    public static function getCurrentUserData()
    {
        return static::$topCallers['current_user'];
    }

    public static function init(array $chartData, $currentUserId)
    {
        static::$rawData = $chartData;
        static::$currentUserId = $currentUserId;
        $idAndDurationArray = parent::map($chartData, 'operator_id', 'duration_total');
        arsort($idAndDurationArray);
        $firstAndSecondByDuration = array_slice($idAndDurationArray,0,2,true);

        list($leaderOperatorId, $secondOperatorId) = array_keys($firstAndSecondByDuration);

        foreach ($chartData as $dataItem) {
            if ((integer)$dataItem['operator_id'] === (integer)$leaderOperatorId) {
                static::$topCallers['leader'] = $dataItem;
            } elseif ((integer)$dataItem['operator_id'] === (integer)$secondOperatorId) {
                static::$topCallers['second'] = $dataItem;
            }
            if ((integer)$dataItem['operator_id'] === (integer)$currentUserId) {
                static::$topCallers['current_user'] = $dataItem;
            }
        }
        return static::$topCallers;
    }


    /**
     * Статический метод возвращает массив для построения "колоночного" графика длительности разговоров.
     * @param array $dataRow CdrAnsweredOnly::search() имеет структуру:
     * [
     *  'operator_id' => @int,
     *  'sip_account' => @string,
     *  'user_name' => @string,
     *  'inner_phone' => @string,
     *  'duration_incoming' => @int seconds,
     *  'duration_outgoing' => @int seconds,
     * 	'duration_total' => @int seconds,
     * 	'count_incoming' => @int count,
     * 	'count_outgoing' => @int count,
     * 	'count_total' => @int count,
     * 	'average_incoming_time' => @int average seconds for one call,
     * 	'average_outgoing_time' => @int average seconds for one call,
     * ]
     * @return array элементы в котором являются значениями для построения графика
     */
    public static function getDurationColumnRow(array $dataRow)
    {
        $durationRow = ['My Num', 0, 0, 0, 'opacity: 0']; //default values

        if (0 !== count($dataRow)) {
            $userName = parent::getValue($dataRow, 'user_name');
            $userPhone = parent::getValue($dataRow, 'inner_phone');
            $totalDuration = parent::getValue($dataRow, 'duration_total');

            $durationRow = [
                static::getFormattedUserNameAndPhone($userName, $userPhone),
                round((integer)parent::getValue($dataRow, 'duration_incoming')/60/60, 2),
                round((integer)parent::getValue($dataRow, 'duration_outgoing')/60/60, 2),
                round((integer)parent::getValue($dataRow, 'duration_total')/60/60, 2),
                static::getOpacity($totalDuration),
            ];
        }
        return $durationRow;
    }

    /**
     * @param string $userName
     * @param string $userPhone
     * @return string
     */
    private static function getFormattedUserNameAndPhone($userName = '', $userPhone = '')
    {
        return $userName ? $userName .' ('. $userPhone . ')' : $userPhone;
    }

    private static function getOpacity($totalDuration)
    {
        $targetDuration = array_key_exists('targetDailyDuration', Yii::$app->getModule('reports')->params)
            ? Yii::$app->getModule('reports')->params['targetDailyDuration']
            : 7200;
        return 'opacity: ' . round((integer)$totalDuration / (integer)$targetDuration, 2);
    }

    public static function getDurationPieRow(array $dataRow)
    {
        $userName = parent::getValue($dataRow, 'user_name');
        $userPhone = parent::getValue($dataRow, 'inner_phone');
        $totalDuration = parent::getValue($dataRow, 'duration_total');

        $durationArray = [
                static::getFormattedUserNameAndPhone($userName, $userPhone),
                round((integer)$totalDuration/60/60, 2),
        ];

        return $durationArray;
    }

    public static function getCompanyLabel()
    {
        return array_key_exists('companyName', Yii::$app->getModule('reports')->params)
            ? Yii::$app->getModule('reports')->params['companyName']
            : 'My Company';
    }

    /**
     * @param string $what
     * @return int
     */
    public static function getSum($what = '')
    {
        $sum = 0;
        foreach (static::$rawData as $item) {
            $sum += $item[$what];
        }
        return $sum;
    }

    public static function getCountColumnRow(array $dataRow)
    {
        $countRow = ['My Num', 0, 0, 0,]; //default values

        if (0 !== count($dataRow)) {
            $userName = parent::getValue($dataRow, 'user_name');
            $userPhone = parent::getValue($dataRow, 'inner_phone');

            $countRow = [
                static::getFormattedUserNameAndPhone($userName, $userPhone),
                (integer)parent::getValue($dataRow, 'count_incoming'),
                (integer)parent::getValue($dataRow, 'count_outgoing'),
                (integer)parent::getValue($dataRow, 'count_total'),
            ];
        }
        return $countRow;
    }

    public static function getCountPieRow(array $dataRow)
    {
        $userName = parent::getValue($dataRow, 'user_name');
        $userPhone = parent::getValue($dataRow, 'inner_phone');
        $totalCount = parent::getValue($dataRow, 'count_total');

        $countPieArray = [
            static::getFormattedUserNameAndPhone($userName, $userPhone),
            (integer)$totalCount,
        ];

        return $countPieArray;
    }
}