<?php

namespace app\commands;
use app\rbac\DepartmentRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $departmentRule = new DepartmentRule();
        $auth->add($departmentRule);

        $viewRecords = $auth->createPermission('viewRecords');
        $viewRecords->description = 'View selected users CDR';
        $viewRecords->ruleName = $departmentRule->name;
        $auth->add($viewRecords);

        $createRecordsMetaInfo = $auth->createPermission('createRecordsMetaInfo');
        $createRecordsMetaInfo->description = 'Create additional information about record';
        $auth->add($createRecordsMetaInfo);

        $updateRecordsMetaInfo = $auth->createPermission('updateRecordsMetaInfo');
        $updateRecordsMetaInfo->description = 'Update additional information about record';
        $auth->add($updateRecordsMetaInfo);

        $caller = $auth->createRole('caller');
        $auth->add($caller);
        $auth->addChild($caller, $viewRecords);

        $callerMaster = $auth->createRole('callerMaster');
        $auth->add($callerMaster);
        $auth->addChild($callerMaster, $caller);
        $auth->addChild($callerMaster, $createRecordsMetaInfo);
        $auth->addChild($callerMaster, $updateRecordsMetaInfo);

        $department = $auth->createRole('department');
        $auth->add($department);
        $auth->addChild($department, $callerMaster);

        $company = $auth->createRole('company');
        $auth->add($company);
        $auth->addChild($company, $department);



        $listenRecords = $auth->createPermission('listenRecords');
        $listenRecords->description = 'Listen records';
        $auth->add($listenRecords);

    }

    public function actionDelete()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }

    public function actionDebug()
    {
        $auth = Yii::$app->authManager;
        var_dump($auth);
    }
}