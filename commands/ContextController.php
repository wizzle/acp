<?php

namespace app\commands;

use yii\console\Controller;
use app\models\sipconfig\SipContext;
use app\models\Cdr;
use yii\console\Exception;
use yii\helpers\ArrayHelper;

/**
 * This command fills contexts table from all availible contexts in CDR table.
 */
class ContextController extends Controller
{

    public function actionUpdate()
    {
        $contextsToSaveArray = array_diff($this->getAllContextsFromCdr(), $this->getAllActualContexts());
        if (count($contextsToSaveArray) === 0) {
            die ('Sorry! New contexts is not found.' . PHP_EOL);
        }

        foreach ($contextsToSaveArray as $context) {
            echo 'Trying to add context: \'' . $context .'\''. PHP_EOL;
            try {
                $c = new SipContext();
                $c->context_name = $context;
                $c->description = 'This context is automatically added.';
                $c->save();
            } catch (Exception $e) {
                echo $e;
            }
            echo 'Context: \'' . $context . '\' successfully added!' . PHP_EOL;
        }
    }

    private function getAllContextsFromCdr()
    {
        $contexts = Cdr::find()->select(['dcontext'])->where(['disposition' => 'ANSWERED'])->groupBy('dcontext')->all();
        return ArrayHelper::getColumn($contexts, function ($element){
            return $element['dcontext'];
        });
    }

    private function getAllActualContexts()
    {
        $contexts = SipContext::find()->select(['context_name'])->all();
        return ArrayHelper::getColumn($contexts, function ($element){
            return $element['context_name'];
        });
    }
}
