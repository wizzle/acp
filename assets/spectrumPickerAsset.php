<?php

namespace app\assets;

use yii\web\AssetBundle;

class spectrumPickerAsset extends AssetBundle
{
	public $js = [
		'js/spectrum/spectrum.js',
	];
	public $css = [
		'css/spectrum/spectrum.css',
	];
	public $depends = [
		'yii\web\JqueryAsset',
	];

} 