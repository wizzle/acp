<?php

namespace app\assets;

use yii\web\AssetBundle;

class searchAsset extends AssetBundle
{
	public $css = [
		'css/search.css',
	];
}