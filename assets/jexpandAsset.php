<?php

namespace app\assets;

use yii\web\AssetBundle;

class jexpandAsset extends AssetBundle
{
	public $js = [
		'js/tableAccordeon.js',
		'js/cdrdetails.js',
	];
	public $css = [
		'css/cdrdetails.css',
	];
	public $depends = [
		'yii\web\JqueryAsset',
	];

} 